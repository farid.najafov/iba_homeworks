package random;

import java.util.Arrays;

public class B {
    
    private static boolean check(String a, String b) {
        a = a.replaceAll(" ", "").toLowerCase();
        b = b.replaceAll(" ", "").toLowerCase();
        if (a.length() != b.length()) return false;

        int sum = 0;
        for (char c = 'a'; c <= 'z'; c++) {
            for (int i = 0; i < a.length(); i++) {
                if (a.charAt(i) == c) sum++;
                if (b.charAt(i) == c) sum--;
            }
            if (sum != 0) return false;
        }
        return true;
    }
    
    private static int findDifferentNumber(int[] a, int[] b) {
        int diff = 0;
        for (int i = 0; i < Math.max(a.length, b.length); i++) {
            if (i < a.length) diff += a[i];
            if (i < b.length) diff -= b[i];
        }
        return Math.abs(diff);
    }
    
    public static void main(String[] args) {
        System.out.println(check(" asdf ", "fdsa  "));
        System.out.println(findDifferentNumber(new int[]{1,2,3}, new int[]{1,2,3,4}));
    }
}
