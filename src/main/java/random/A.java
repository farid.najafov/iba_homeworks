package random;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class A {
    
    private static String reverse(String name) {
        int len = name.length();
        for (int i = 0; i < len - 1; i++) {
            name = name.substring(1, len - i) +
                    name.substring(0, 1) +
                    name.substring(len - i);
        }
        return name;
    }
    
    private static String reverse1(String name) {
        int len = name.length();
        String s = "";
        for (int i = 0; i < len; i++) {
//            s += String.join("", name.substring(len - i - 1, len - i));
            s += String.format("%s", name.substring(len - i - 1, len - i));
        }
        return s;
    }
    
    private static String reverse2(String name) {
        int len = name.length();
        for (int i = 0; i < len - 1; i++) {
            name = String.format("%s%s%s", name.substring(1, len - i),
                    name.substring(0, 1),
                    name.substring(len - i));
        }
        return name;
    }
    
    private static String reverse3(String name) {
        List<Character> collect = name.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        Collections.reverse(collect);
        name = collect.stream().map(String::valueOf).collect(Collectors.joining(""));
        return name;
    }
    
    private static String reverse4(String name) {
        return new StringBuilder(name).reverse().toString();
    }
    
    private static String reverse5(String name) {
        int len = name.length();
        for (int i = 0; i < len; i++) {
            name += name.charAt(len - i - 1);
        }
        return name.substring(len);
    }
    
    private static String reverse6(String name) {
        int len = name.length();
        String s = "";
        for (int i = 0; i < len; i++) {
            s += name.charAt(len - i - 1);
        }
        return s;
    }
    
    public static void main(String[] args) {
        long start = System.nanoTime();
        System.out.println(reverse("qwerty"));
        System.out.println((double) (System.nanoTime() - start) / 1_000_000_000);
        
        System.out.println("======================");
        
        long start1 = System.nanoTime();
        System.out.println(reverse1("qwerty"));
        System.out.println((double) (System.nanoTime() - start1) / 1_000_000_000);
        
        System.out.println("======================");
        
        long start2 = System.nanoTime();
        System.out.println(reverse2("qwerty"));
        System.out.println((double) (System.nanoTime() - start2) / 1_000_000_000);
        
        System.out.println("======================");
        
        long start3 = System.nanoTime();
        System.out.println(reverse3("qwerty"));
        System.out.println((double) (System.nanoTime() - start3) / 1_000_000_000);
        
        System.out.println("======================");
        
        long start4 = System.nanoTime();
        System.out.println(reverse4("qwerty"));
        System.out.println((double) (System.nanoTime() - start4) / 1_000_000_000);
        
        System.out.println("======================");
        
        long start5 = System.nanoTime();
        System.out.println(reverse5("qwerty"));
        System.out.println((double) (System.nanoTime() - start5) / 1_000_000_000);
        
        System.out.println("======================");
        
        long start6 = System.nanoTime();
        System.out.println(reverse4("qwerty"));
        System.out.println((double) (System.nanoTime() - start6) / 1_000_000_000);
        
    }
}

