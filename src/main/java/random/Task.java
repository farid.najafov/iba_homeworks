package random;

public class Task {
    public static boolean check(String a, String b) {
        int[] array = new int[26];
        
        for (int c = 0; c < Math.max(a.length(), b.length()); c++) {
            if (c < a.length() && a.charAt(c) != (char) 32) array[a.charAt(c)-'a']++;
            if (c < b.length() && b.charAt(c) != (char) 32) array[b.charAt(c)-'a']--;
        }
        
        for (int i : array) {
            if (i != 0) return false;
        }
        
        return true;
    }
    
    public static void main(String[] args) {
        System.out.println(check(" asdf", "fd sa"));
    }
}
