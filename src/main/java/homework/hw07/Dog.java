package homework.hw07;

import java.util.Arrays;

public class Dog extends Pet03{
    Species03 species = Species03.Dog;

    public Dog(String nickname, int age, byte trickLevel, String[] habits) {
        super(nickname, age, trickLevel, habits);
    }

    void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", getNickname());
    }

    @Override
    public String toString() {
        return "Dog{" +
                "species='" + species + '\'' +
                ", nickname='" + super.getNickname() + '\'' +
                ", age=" + super.getAge() +
                ", trickLevel=" + super.getTrickLevel() +
                ", habits=" + Arrays.toString(super.getHabits()) +
                '}';
    }

}
