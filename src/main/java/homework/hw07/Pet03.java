package homework.hw07;

abstract public class Pet03 {
    private Species03 species = Species03.UNKNOWN;
    private String nickname;
    private int age;
    private byte trickLevel;
    private String[] habits;

    //  constructors
    public Pet03(String nickname, int age, byte trickLevel, String[] habits){
        this.nickname = nickname;
        this.age = age;

        if (trickLevel >= 1 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else {
            throw new IllegalArgumentException("trickLevel must be a whole number from 1 to 100!");
        }

        this.habits = habits;
    }

    //  methods
    void eat() {
        System.out.println("I am eating");
    }
    abstract void respond();

    //  getters and setters
    public Species03 getSpecies() {
        return species;
    }
    public void setSpecies(Species03 species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(byte trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
}
