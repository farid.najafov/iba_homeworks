package homework.hw07;

import java.util.Arrays;
import java.util.Objects;

public class Family03 {
    private Human03 mother;
    private Human03 father;
    private Human03[] children;
    private Pet03 pet;

    //  constructors
    Family03() {

    }
    Family03(Human03 mother, Human03 father, Human03[] children, Pet03 pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    //    methods
    void describePet() {
        if (pet.getAge() < 50) {
            System.out.printf("I have a %s, he is %s years old, he is very sly\n", pet.getSpecies(), pet.getAge());
        } else {
            System.out.printf("I have a %s, he is %s years old, he is almost not sly\n", pet.getSpecies(), pet.getAge());
        }
    }
    void addChild (Human03 child) {
        if(child == null) throw new IllegalArgumentException("Child cannot be null");

        if(this.children == null) {
            this.children = new Human03[] {child};
        } else {
            Human03[] newArray = new Human03[this.children.length + 1];
            for (int i = 0; i < children.length; i++ ){
                newArray[i] = this.children[i];
            }
            newArray[newArray.length-1] = child;
            this.children = newArray;
        }
    }
    boolean deleteChild (Human03 child) {
        if(child == null) throw new IllegalArgumentException("Child cannot be null!");
        boolean isDeleted = false;
        for (int k = 0; k < children.length; k++) {
            if (children[k] == child) {
                Human03[] newChildren = new Human03[children.length - 1];
                int countChildren = 0;
                for (Human03 ch : children) {
                    if (ch != child) {
                        newChildren[countChildren++] = ch;
                    } else
                        isDeleted = true;
                }
                children = newChildren;
            }
        }
        return  isDeleted;
    }
    int countFamily () {
        return 2 + children.length;
    }

    //  getters and setters
    public Human03 getMother() {
        return mother;
    }
    public void setMother(Human03 mother) {
        this.mother = mother;
    }

    public Human03 getFather() {
        return father;
    }
    public void setFather(Human03 father) {
        this.father = father;
    }

    public Human03[] getChildren() {
        return children;
    }
    public void setChildren(Human03[] children) {
        this.children = children;
    }

    public Pet03 getPet() {
        return pet;
    }
    public void setPet(Pet03 pet) {
        this.pet = pet;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family03 family03 = (Family03) o;
        return mother.equals(family03.mother) &&
                father.equals(family03.father) &&
                Arrays.equals(children, family03.children) &&
                pet.equals(family03.pet);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
    @Override
    public String toString() {
        return "Family01{" +
                "mother=" + mother + "\n" +
                ", father=" + father + "\n" +
                ", children=" + Arrays.toString(children) + "\n" +
                ", pet=" + pet +
                '}';
    }
}
