package homework.hw07;

public final class Woman extends Human03{
    public Woman(String name, String surname, int yearOfBirth, byte iq, Family03 family) {
        super(name, surname, yearOfBirth, iq, family);
    }

    @Override
    void greetPet() {
        System.out.printf("Hello, %s\n", super.getFamily().getPet().getNickname());
    }

    void makeUp() {
        System.out.println("I'm wearing makeup");
    }
}
