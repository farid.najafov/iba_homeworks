package homework.hw07;

import java.util.Arrays;
import java.util.Objects;

public class Human03 {
    private String name;
    private String surname;
    private int year;
    private byte iq;
    private String[][] schedule;
    private Family03 family;

    //  constructors
    public Human03(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;
    }
    public Human03(String name, String surname, int yearOfBirth, byte iq, Family03 family) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;

        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }

        this.family = family;
    }

    //  methods
    void greetPet(){

    };

    //  getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }
    public void setIq(byte iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family03 getFamily() {
        return family;
    }
    public void setFamily(Family03 family) {
        this.family = family;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human03 human03 = (Human03) o;
        return year == human03.year &&
                iq == human03.iq &&
                Objects.equals(name, human03.name) &&
                Objects.equals(surname, human03.surname) &&
                Arrays.equals(schedule, human03.schedule) &&
                Objects.equals(family, human03.family);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }
    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                '}';
    }
}
