package homework.hw07;

public enum Species03 {
    DomesticCat,
    Fish,
    Dog,
    RoboCat,
    UNKNOWN,
}
