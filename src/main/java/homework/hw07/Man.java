package homework.hw07;

public final class Man extends Human03 {

    public Man(String name, String surname, int yearOfBirth, byte iq, Family03 family) {
        super(name, surname, yearOfBirth, iq, family);
    }

    @Override
    void greetPet() {
        System.out.printf("Hello, %s\n", super.getFamily().getPet().getNickname());
    }

    void repairCar() {
        System.out.println("I'm fixing my car");
    }
}
