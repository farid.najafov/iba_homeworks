package homework.hw07;

public class Main03 {
    public static void main(String[] args) {
        // create a cat
        DomesticCat myCat = new DomesticCat("Kitty", 5, (byte) 15, new String[]{"eat", "drink", "sleep"});
        myCat.respond();
        myCat.foul();
        System.out.println(myCat);

        // create a dog
        Dog myDog = new Dog("Rock", 3, (byte) 25, new String[]{"run", "play", "sleep"});
        myDog.respond();
        myDog.foul();
        System.out.println(myDog);

        // create fish
        Fish myFish = new Fish("Nemo", 2, (byte) 5, new String[]{"swim", "eat"});
        System.out.println(myFish);

        Family03 family = new Family03();

        // create a man
        Man man = new Man("John", "Travolta", 1950, (byte) 90, family);
        man.repairCar();
        System.out.println(man);

        // create a woman
        Woman woman = new Woman("Jenny", "Oldstones", 1970, (byte) 70, family);
        woman.makeUp();
        System.out.println(woman);

    }
}
