package homework.hw13.entity;

import java.io.Serializable;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public final class Woman extends Human implements Serializable {
    private static final long serialVersionUID = 1L;
    public Woman(String name, String surname, byte iq, String birthDate) {
        super(name, surname, iq, birthDate);
    }

    public Woman(String name, String surname, String birthDate) {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, byte iq, Map<DayOfWeek, String> schedule)  {
        super(name, surname, birthDate, iq, schedule);
    }

    public Woman() {
    }

    @Override
    public String prettyFormat() {
        return "    girl: {name='" + super.getName() +
                ", surname='" + super.getSurname() +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(super.getBirthDate())
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + super.getIq() +
                ", schedule=" + super.getSchedule() + "}\n";
    }

    void makeUp() {
        System.out.println("I'm wearing makeup");
    }

    @Override
    public String toString() {
        return "{name='" + super.getName() +
                ", surname='" + super.getSurname() +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(super.getBirthDate())
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + super.getIq() +
                ", schedule=" + super.getSchedule() + "}\n";
    }
}
