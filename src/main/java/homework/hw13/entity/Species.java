package homework.hw13.entity;

public enum Species {
    DomesticCat,
    Fish,
    Dog,
    RoboCat,
    UNKNOWN
}
