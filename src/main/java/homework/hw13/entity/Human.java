package homework.hw13.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Objects;

public class Human implements Serializable {
    private static final long serialVersionUID = 1L;
    private String name;
    private String surname;
    private long birthDate;
    private byte iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;

    //  constructors
    public Human(String name, String surname, byte iq, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        try{
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException ignored){}

    }

    public Human(String name, String surname, String birthDate){
        this.name = name;
        this.surname = surname;
        try{
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException ignored){}
    }
    public Human(String name, String surname, String birthDate, byte iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        try{
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException ignored){}
        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }

        this.schedule = schedule;
    }

    public Human() {

    }

    //methods
    public int getAge(){
        LocalDate currentDate = LocalDate.now();
        LocalDate birthDate = LocalDate.ofEpochDay(this.birthDate / 86400000L);
        return (int) ChronoUnit.YEARS.between(birthDate, currentDate);//currentYear - this.birthdate;
    }

    public String describeAge(){
        LocalDate currentDate = LocalDate.now();
        LocalDate birthDate = LocalDate.ofEpochDay(this.birthDate / 86400000L);
        long yearsBetween = ChronoUnit.YEARS.between(birthDate, currentDate);
        long monthsBetween = ChronoUnit.MONTHS.between(birthDate, currentDate);
        long daysBetween = ChronoUnit.DAYS.between(birthDate, currentDate);
        return "years = " + yearsBetween + ", months = " + monthsBetween + ", days = " + daysBetween;
    }

    public String prettyFormat() {
        return "{name='" + name +
                ", surname='" + surname +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(birthDate)
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + iq +
                ", schedule=" + schedule + "}\n";
    }

    //  getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) {
        try{
            this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
        } catch (ParseException ignored){}
    }

    public int getIq() {
        return iq;
    }
    public void setIq(byte iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }
    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }

    // overridden methods

    @Override
    public String toString() {
        return "{name='" + name +
                ", surname='" + surname +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(birthDate)
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + iq +
                ", schedule=" + schedule + "}\n";
    }
}

