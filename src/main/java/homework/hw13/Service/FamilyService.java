package homework.hw13.Service;

import homework.hw13.DAO.CollectionFamilyDAO;
import homework.hw13.DAO.FamilyDAO;
import homework.hw13.entity.*;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService {
    FamilyDAO familyDAO = new CollectionFamilyDAO();

    public List<Family> getAllFamilies() throws ParseException {
        return familyDAO.getAllFamilies();
    }

    public void saveFamily(Family family) throws ParseException {
        familyDAO.saveFamily(family);
    }

    public void displayAllFamilies() throws ParseException {
//        familyDAO.getAllFamilies().stream().map(Family::prettyFormat).forEach(System.out::println);
        for (int i = 0; i < familyDAO.getAllFamilies().size(); i++) {
            System.out.print(i + 1 + ") ");
            System.out.println(familyDAO.getAllFamilies().get(i).prettyFormat());

        }
    }

    public List<Family> getFamiliesBiggerThan(int numOfMembers) throws ParseException {
        return familyDAO.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > numOfMembers)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int numOfMembers) throws ParseException {
        return familyDAO.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < numOfMembers)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int numOfMembers) throws ParseException {
         return (int) familyDAO.getAllFamilies()
                 .stream()
                 .filter(family -> family.countFamily() == numOfMembers)
                 .count();
    }

    public void createNewFamily(Human father, Human mother) throws ParseException {
        familyDAO.saveFamily(new Family(father, mother,new ArrayList<>(), new HashSet<>()));
    }

    public void deleteFamilyByIndex(int familyIndex) throws ParseException {
        familyDAO.deleteFamily(familyIndex - 1);
    }

    public Family bornChild(Family family, String maleName, String femaleName) throws ParseException {
        Human child = (int) (Math.random()*10) == 0 ? new Man() : new Woman();
        if (child instanceof Man) {
            child.setName(maleName);
        } else child.setName(femaleName);

            familyDAO.getAllFamilies().stream().filter(family1 -> family1.equals(family))
                    .findFirst().filter(family1 -> family1.getChildren() != null);
            family.getChildren().add(child);
            saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) throws ParseException {
        if (family.getChildren() != null) {
            family.getChildren().add(child);
        } else {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(child);
        }
        saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) throws ParseException {
        familyDAO.getAllFamilies()
                .stream()
                .filter(family -> family.getChildren() != null)
                .forEach(family -> family.getChildren()
                    .removeIf(child -> child.getAge() > age));
    }

    public int count() throws ParseException {
        return familyDAO.getAllFamilies().size();
    }

    public Family getFamilyById(int familyId) throws ParseException {
        return familyDAO.getFamilyByIndex(familyId - 1);
    }

    public Set<Pet> getPets(int familyIndex) throws ParseException {
        if (familyIndex < familyDAO.getAllFamilies().size() && familyIndex >= 0) {
            return familyDAO.getAllFamilies().get(familyIndex - 1).getPet();
        } else throw new IndexOutOfBoundsException("no family with this index is found");
    }

    public void addPet(int familyIndex, Pet pet) throws ParseException {
        familyDAO.getAllFamilies().get(familyIndex - 1).getPet().add(pet);
    }
}
