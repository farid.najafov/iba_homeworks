package homework.hw13.DAO;

import homework.hw13.entity.*;

import java.io.*;
import java.text.ParseException;
import java.util.*;

public class CollectionFamilyDAO implements FamilyDAO {
    File file = new File("./src/main/java/homework/hw13/files/families.txt");

    @Override
    public List<Family> getAllFamilies() throws ParseException {
        try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            Object read = ois.readObject();
            List<Family> objects = (ArrayList<Family>) read;
            return objects;
        } catch (IOException | ClassNotFoundException ex) {
            return testData();
        }
    }
    @Override
    public Family getFamilyByIndex(int index) throws ParseException {
        return getAllFamilies().get(index);
    }
    @Override
    public void deleteFamily(int index) throws ParseException {
        List<Family> families = getAllFamilies();
        families.remove(index);
        write(families);
    }
    @Override
    public void saveFamily(Family family) throws ParseException {
       List<Family> families = getAllFamilies();
        families.add(family);
        write(families);
    }
    public void write(List<Family> families) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
            oos.writeObject(families);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("DAO:write:IOException", ex);
        }
    }
    public List<Family> testData() {
        //      =========================== create family Reynold ====================================
        // create pets
        DomesticCat myCat = new DomesticCat("Kitty");
        Dog myDog = new Dog("Rock");
        Set<Pet> petsOfReynolds = new HashSet<>();
        petsOfReynolds.add(myCat);
        petsOfReynolds.add(myDog);

        // create father and mother
        Human fatherRey = new Man("Ryan", "Reynolds", "01/01/1990");
        Human motherRey = new Woman("Blake", "Lively", "01/01/1990");

        // create children
        Human firstChildRey = new Man("James", "Reynolds", "01/01/2010");
        Human secondChildRey = new Woman("Inez", "Reynolds", "01/01/2015");
        List<Human> childrenRey = new ArrayList<>();
        childrenRey.add(firstChildRey);
        childrenRey.add(secondChildRey);

        // create family
        Family fmReynolds = new Family(motherRey, fatherRey, childrenRey, petsOfReynolds);

//      =========================== create family Eastwoods ====================================
        // create pets
        Fish myFish = new Fish("Nemo");
        RoboCat myRoboCat = new RoboCat("Bob");
        Set<Pet> petsOfEastwoods = new HashSet<>();
        petsOfEastwoods.add(myFish);
        petsOfEastwoods.add(myRoboCat);

        // create father and mother
        Human fatherEast = new Man("Clint", "Eastwood", "01/01/1940");
        Human motherEast = new Woman("Dina", "Eastwood", "01/01/1960");

        // create children
        Human firstChildEast = new Man("Scott", "Eastwood", "01/01/1990");
        Human secondChildEast = new Woman("Morgan", "Eastwood", "01/01/2000");
        List<Human> childrenEast = new ArrayList<>();
        childrenEast.add(firstChildEast);
        childrenEast.add(secondChildEast);

        // create family
        Family fmEastwoods = new Family(motherEast, fatherEast, childrenEast, petsOfEastwoods);

        List<Family> families = new ArrayList<>();
        families.add(fmReynolds);
        families.add(fmEastwoods);
        write(families);
        return families;
    }

}
