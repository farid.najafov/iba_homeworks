package homework.hw13.DAO;

import homework.hw13.entity.Family;

import java.text.ParseException;
import java.util.List;

public interface FamilyDAO {

    List<Family> getAllFamilies() throws ParseException;
    Family getFamilyByIndex(int index) throws ParseException;
    void deleteFamily(int index) throws ParseException;
    void saveFamily(Family family) throws ParseException;
}

