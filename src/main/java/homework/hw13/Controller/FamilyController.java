package homework.hw13.Controller;

import homework.hw13.Service.FamilyService;
import homework.hw13.entity.Family;
import homework.hw13.entity.Human;
import homework.hw13.entity.Pet;
import homework.hw13.exception.FamilyOverflowException;

import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService familyService = new FamilyService();

    public Collection<Family> getAllFamilies() throws ParseException {
        return familyService.getAllFamilies();
    }

    public void saveFamily(Family family) throws ParseException {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() throws ParseException {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numOfMembers) throws ParseException {
        return familyService.getFamiliesBiggerThan(numOfMembers);
    }

    public List<Family> getFamiliesLessThan(int numOfMembers) throws ParseException {
        return familyService.getFamiliesLessThan(numOfMembers);
    }

    public int countFamiliesWithMemberNumber(int numOfMembers) throws ParseException {
        return familyService.countFamiliesWithMemberNumber(numOfMembers);
    }

    public void createNewFamily(Human father, Human mother) throws ParseException {
        familyService.createNewFamily(father, mother);
    }

    public void deleteFamilyByIndex(int familyIndex) throws ParseException {
        familyService.deleteFamilyByIndex(familyIndex);
    }

    public Family bornChild(Family family, String maleName, String femaleName)  {
        try {
            getAllFamilies().stream().filter(p -> p.equals(family)).
                    findFirst().filter(a -> a.countFamily() < 7);
            return familyService.bornChild(family, maleName, femaleName);
        } catch (FamilyOverflowException | ParseException e) {
            System.out.println("Sorry, your have reached the maximum number of family members");
        }
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        try {
            getAllFamilies().stream().filter(p -> p.equals(family))
                    .findFirst().filter(a -> a.countFamily() < 7).orElseThrow(() -> new FamilyOverflowException(""));
            return familyService.adoptChild(family, child);
        } catch (FamilyOverflowException | ParseException e) {
            System.out.println("Sorry, your have reached the maximum number of family members");
        }
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) throws ParseException {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() throws ParseException {
        return familyService.count();
    }

    public Family getFamilyById(int familyId) throws ParseException {
        return familyService.getFamilyById(familyId);
    }

    public Set<Pet> getPets(int familyIndex) throws ParseException {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) throws ParseException {
        familyService.addPet(familyIndex, pet);
    }

}
