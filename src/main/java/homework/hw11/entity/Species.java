package homework.hw11.entity;

public enum Species {
    DomesticCat,
    Fish,
    Dog,
    RoboCat,
    UNKNOWN
}
