package homework.hw12.entity;

public enum Species {
    DomesticCat,
    Fish,
    Dog,
    RoboCat,
    UNKNOWN
}
