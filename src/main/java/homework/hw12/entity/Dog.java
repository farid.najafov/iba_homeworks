package homework.hw12.entity;

import java.util.Set;

public class Dog extends Pet {
    Species species = Species.Dog;

    public Dog(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    public Dog(String nickname) {
        super(nickname);
    }

    void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", getNickname());
    }

    @Override
    String prettyFormat() {
       return   "{species='" + species + '\'' +
                ", nickname='" + super.getNickname() + '\'' +
                ", age=" + super.getAge() +
                ", trickLevel=" + super.getTrickLevel() +
                ", habits=" + super.getHabits() +
                "}";
    }

    @Override
    public String toString() {
        return "{species='" + species + '\'' +
                ", nickname='" + super.getNickname() + '\'' +
                ", age=" + super.getAge() +
                ", trickLevel=" + super.getTrickLevel() +
                ", habits=" + super.getHabits() +
                '}';
    }

}
