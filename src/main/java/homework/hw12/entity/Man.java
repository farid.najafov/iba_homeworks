package homework.hw12.entity;

import java.text.ParseException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public final class Man extends Human {
    public Man(String name, String surname, byte iq, String birthDate) throws ParseException {
        super(name, surname, iq, birthDate);
    }

    public Man(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, byte iq, Map<DayOfWeek, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man() {
    }

    void repairCar() {
        System.out.println("I'm fixing my car");
    }

    @Override
    public String prettyFormat() {
        return "    boy: {name='" + super.getName() +
                ", surname='" + super.getSurname() +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(super.getBirthDate())
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + super.getIq() +
                ", schedule=" + super.getSchedule() + "}\n";
    }

    @Override
    public String toString() {
        return "{name='" + super.getName() +
                ", surname='" + super.getSurname() +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(super.getBirthDate())
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + super.getIq() +
                ", schedule=" + super.getSchedule() + "}\n";
    }
}
