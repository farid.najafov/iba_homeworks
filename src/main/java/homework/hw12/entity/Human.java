package homework.hw12.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private byte iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;

    //  constructors
    public Human(String name, String surname, byte iq, String birthDate) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
    }

    public Human(String name, String surname, String birthDate) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
    }
    public Human(String name, String surname, String birthDate, byte iq, Map<DayOfWeek, String> schedule) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();;

        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }

        this.schedule = schedule;
    }

    public Human() {

    }

    //methods
    public int getAge(){
        LocalDate currentDate = LocalDate.now();
        LocalDate birthDate = LocalDate.ofEpochDay(this.birthDate / 86400000L);
        return (int) ChronoUnit.YEARS.between(birthDate, currentDate);//currentYear - this.birthdate;
    }

    public String describeAge(){
        LocalDate currentDate = LocalDate.now();
        LocalDate birthDate = LocalDate.ofEpochDay(this.birthDate / 86400000L);
        long yearsBetween = ChronoUnit.YEARS.between(birthDate, currentDate);
        long monthsBetween = ChronoUnit.MONTHS.between(birthDate, currentDate);
        long daysBetween = ChronoUnit.DAYS.between(birthDate, currentDate);
        return "years = " + yearsBetween + ", months = " + monthsBetween + ", days = " + daysBetween;
    }

    public String prettyFormat() {
        return "{name='" + name +
                ", surname='" + surname +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(birthDate)
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + iq +
                ", schedule=" + schedule + "}\n";
    }

    //  getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }
    public void setBirthDate(String birthDate) throws ParseException {
        this.birthDate = new SimpleDateFormat("dd/MM/yyyy").parse(birthDate).getTime();
    }

    public int getIq() {
        return iq;
    }
    public void setIq(byte iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }
    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDate == human.birthDate &&
                iq == human.iq &&
                name.equals(human.name) &&
                surname.equals(human.surname) &&
                schedule.equals(human.schedule) &&
                family.equals(human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDate, iq, schedule, family);
    }

    @Override
    public String toString() {
        return "{name='" + name +
                ", surname='" + surname +
                ", birthDate=" + DateTimeFormatter.ofPattern("dd/MM/yyyy")
                .format(Instant.ofEpochMilli(birthDate)
                        .atZone(ZoneId.systemDefault()).toLocalDate()) +
                ", iq=" + iq +
                ", schedule=" + schedule + "}\n";
    }
}

