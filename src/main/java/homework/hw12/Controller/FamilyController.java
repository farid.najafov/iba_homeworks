package homework.hw12.Controller;

import homework.hw12.Service.FamilyService;
import homework.hw12.entity.Family;
import homework.hw12.entity.Human;
import homework.hw12.entity.Pet;
import homework.hw12.exception.FamilyOverflowException;

import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numOfMembers) {
        return familyService.getFamiliesBiggerThan(numOfMembers);
    }

    public List<Family> getFamiliesLessThan(int numOfMembers) {
        return familyService.getFamiliesLessThan(numOfMembers);
    }

    public int countFamiliesWithMemberNumber(int numOfMembers) {
        return familyService.countFamiliesWithMemberNumber(numOfMembers);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public boolean deleteFamilyByIndex(int familyIndex) {
        return familyService.deleteFamilyByIndex(familyIndex);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public Family bornChild(Family family, String maleName, String femaleName)  {
        try {
            getAllFamilies().stream().filter(p -> p.equals(family)).
                    findFirst().filter(a -> a.countFamily() < 5).orElseThrow(() -> new FamilyOverflowException(""));
            return familyService.bornChild(family, maleName, femaleName);
        } catch (FamilyOverflowException e) {
            System.out.println("Sorry, your have reached the maximum number of family members");
        }
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        try {
            getAllFamilies().stream().filter(p -> p.equals(family))
                    .findFirst().filter(a -> a.countFamily() < 5).orElseThrow(() -> new FamilyOverflowException(""));
            return familyService.adoptChild(family, child);
        } catch (FamilyOverflowException e) {
            System.out.println("Sorry, your have reached the maximum number of family members");
        }
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int familyId) {
        return familyService.getFamilyById(familyId);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }

}
