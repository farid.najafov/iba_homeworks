package homework.hw12;

import homework.hw12.Controller.FamilyController;
import homework.hw12.entity.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Database {
    private FamilyController fc;
    public Database(FamilyController fc) {
        this.fc = fc;
    }

    public void fillWIthTestData() throws ParseException {
        try {
            //      =========================== create family Reynold ====================================
            // create pets
            DomesticCat myCat = new DomesticCat("Kitty");
            Dog myDog = new Dog("Rock");
            Set<Pet> petsOfReynolds = new HashSet<>();
            petsOfReynolds.add(myCat);
            petsOfReynolds.add(myDog);

            // create father and mother
            Human fatherRey = new Man("Ryan", "Reynolds", "01/01/1990");
            Human motherRey = new Woman("Blake", "Lively", "01/01/1990");

            // create children
            Human firstChildRey = new Man("James", "Reynolds", "01/01/2010");
            Human secondChildRey = new Woman("Inez", "Reynolds", "01/01/2015");
            List<Human> childrenRey = new ArrayList<>();
            childrenRey.add(firstChildRey);
            childrenRey.add(secondChildRey);

            // create family
            Family fmReynolds = new Family(motherRey, fatherRey, childrenRey, petsOfReynolds);

//      =========================== create family Eastwoods ====================================
            // create pets
            Fish myFish = new Fish("Nemo");
            RoboCat myRoboCat = new RoboCat("Bob");
            Set<Pet> petsOfEastwoods = new HashSet<>();
            petsOfEastwoods.add(myFish);
            petsOfEastwoods.add(myRoboCat);

            // create father and mother
            Human fatherEast = new Man("Clint", "Eastwood", "01/01/1940");
            Human motherEast = new Woman("Dina", "Eastwood", "01/01/1960");

            // create children
            Human firstChildEast = new Man("Scott", "Eastwood", "01/01/1990");
            Human secondChildEast = new Woman("Morgan", "Eastwood", "01/01/2000");
            List<Human> childrenEast = new ArrayList<>();
            childrenEast.add(firstChildEast);
            childrenEast.add(secondChildEast);

            // create family
            Family fmEastwoods = new Family(motherEast, fatherEast, childrenEast, petsOfEastwoods);
            fc.saveFamily(fmReynolds);
            fc.saveFamily(fmEastwoods);
        } catch (NullPointerException ignored){}
    }
}
