package homework.hw12.Service;

import homework.hw12.DAO.CollectionFamilyDAO;
import homework.hw12.DAO.FamilyDAO;
import homework.hw12.entity.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    FamilyDAO familyDAO = new CollectionFamilyDAO();

    public List<Family> getAllFamilies() {
        return familyDAO.getAllFamilies();
    }

    public void saveFamily(Family family) {
        familyDAO.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyDAO.getAllFamilies().stream().map(family -> getAllFamilies().indexOf(family)+1+") " + family.prettyFormat())
                .forEach(System.out::println);
    }

    public List<Family> getFamiliesBiggerThan(int numOfMembers) {
        return familyDAO.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > numOfMembers)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int numOfMembers) {
        return familyDAO.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < numOfMembers)
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int numOfMembers) {
         return (int) familyDAO.getAllFamilies()
                 .stream()
                 .filter(family -> family.countFamily() == numOfMembers)
                 .count();
    }

    public void createNewFamily(Human father, Human mother) {
        familyDAO.saveFamily(new Family(father, mother, new ArrayList<>(), new HashSet<>()));
    }

    public boolean deleteFamilyByIndex(int familyIndex) {
        return familyDAO.deleteFamily(familyIndex-1);
    }

    public boolean deleteFamily(Family family) {
        return familyDAO.deleteFamily(family);
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        Human child = (int) (Math.random()*10) == 0 ? new Man() : new Woman();
        if (child instanceof Man) {
            child.setName(maleName);
        } else child.setName(femaleName);

        if (family.getChildren() == null) {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(child);
        } else {
            family.getChildren().add(child);
        }
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        if (family.getChildren() != null) {
            family.getChildren().add(child);
        } else {
            List<Human> children = new ArrayList<>();
            family.setChildren(children);
            children.add(child);
        }
        return family;
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyDAO.getAllFamilies()
                .stream()
                .filter(family -> family.getChildren() != null)
                .forEach(family -> family.getChildren()
                    .removeIf(child -> child.getAge() > age));
    }

    public int count() {
        return familyDAO.getAllFamilies().size();
    }

    public Family getFamilyById(int familyId) {
        return familyDAO.getFamilyByIndex(familyId-1);
    }

    public Set<Pet> getPets(int familyIndex) {
        if (familyIndex < familyDAO.getAllFamilies().size() && familyIndex >= 0) {
            return familyDAO.getAllFamilies().get(familyIndex-1).getPet();
        } else throw new IndexOutOfBoundsException("no family with this index is found");
    }

    public void addPet(int familyIndex, Pet pet) {
        familyDAO.getAllFamilies().get(familyIndex-1).getPet().add(pet);
    }
}
