package homework.hw12.DAO;

import homework.hw12.entity.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDAO implements FamilyDAO {

    public List<Family> familyList = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }

    @Override
    public Family getFamilyByIndex(int familyIndex) {
        return familyIndex < familyList.size() && familyIndex >= 0 ? familyList.get(familyIndex) : null;
    }

    @Override
    public boolean deleteFamily(int familyIndex) {
        if (familyIndex < familyList.size() && familyIndex >= 0) {
            familyList.remove(familyIndex);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (!familyList.contains(family)) return false;
        familyList.remove(family);
        return true;
    }

    @Override
    public void saveFamily(Family family) {
        if (familyList.contains(family)){
            familyList.remove(family);
        }
        familyList.add(family);
    }

}
