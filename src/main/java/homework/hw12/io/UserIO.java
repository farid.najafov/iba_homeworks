package homework.hw12.io;

import homework.hw12.Controller.FamilyController;
import homework.hw12.Database;
import homework.hw12.entity.Family;
import homework.hw12.entity.Human;

import java.text.ParseException;
import java.util.Scanner;

public class UserIO {
    public static void userInput() throws ParseException {
        UnixConsole console = new UnixConsole(new Scanner(System.in));
        FamilyController fc = new FamilyController();
        Database db = new Database(fc);
        String opt;

        while(true) {
            try{
                Menu.showMenu();
                console.printLn("Please choose one of the options:");
                opt = console.readLn();
                int num;
                switch (opt) {
                    case "1":
                        db.fillWIthTestData();
                        console.printLn("Several families has been created");
                        break;
                    case "2":
                        fc.displayAllFamilies();
                        break;
                    case "3":
                        console.printLn("Enter a number:");
                        num = Integer.parseInt(console.readLn());
                        fc.getFamiliesBiggerThan(num).stream().map(Family::prettyFormat).forEach(System.out::println);
                        break;
                    case "4":
                        console.printLn("Enter a number:");
                        num = Integer.parseInt(console.readLn());
                        fc.getFamiliesLessThan(num).stream().map(Family::prettyFormat).forEach(System.out::println);
                        break;
                    case "5":
                        console.printLn("Enter a number:");
                        num = Integer.parseInt(console.readLn());
                        console.printLn(String.valueOf(fc.countFamiliesWithMemberNumber(num)));
                        break;
                    case "6":
                        Human father = Menu.createFather();
                        Human mother = Menu.createMother();
                        fc.createNewFamily(father, mother);
                        break;
                    case "7":
                        console.printLn("Enter an index of the family you want to delete:");
                        num = Integer.parseInt(console.readLn());
                        fc.deleteFamilyByIndex(num);
                        break;
                    case "8":
                        boolean flag = true;
                        while (flag) {
                            Menu.editFamily();
                            console.printLn("Please choose one of the options:");
                            opt = console.readLn();
                            switch (opt) {
                                case "1":
                                    console.printLn("Enter family index:");
                                    num = Integer.parseInt(console.readLn());
                                    Family family = fc.getFamilyById(num);
                                    console.printLn("Enter two names:\n" +
                                            "(if baby is a boy, male name will be given, otherwise female name will be given)");
                                    console.printLn("Male name:");
                                    String maleName = console.readLn();
                                    console.printLn("Female name:");
                                    String femaleName = console.readLn();
                                    fc.bornChild(family, maleName, femaleName);

                                    break;
                                case "2":
                                    console.printLn("Enter family index:");
                                    num = Integer.parseInt(console.readLn());
                                    Family familyN = fc.getFamilyById(num);
                                    Human child = Menu.createChild();
                                    fc.adoptChild(familyN, child);
                                    break;
                                case "3": flag = false;
                                    break;
                                default:
                                    console.printLn("Invalid option. Please try again!");
                            }
                        }
                        break;
                    case "9":
                        console.printLn("Enter the minimum age of the children you want to remove from family:");
                        num = Integer.parseInt(console.readLn());
                        fc.deleteAllChildrenOlderThan(num);
                        break;
                    case "10":
                        System.exit(0);
                    default:
                        console.printLn("Invalid option. Please try again!");
                }
            } catch (NumberFormatException e) {
                console.printLn("Wrong input type, try again:");
            }
        }
    }
}
