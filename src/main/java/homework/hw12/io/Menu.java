package homework.hw12.io;

import homework.hw12.entity.Human;
import homework.hw12.entity.Man;
import homework.hw12.entity.Woman;

import java.text.ParseException;
import java.util.Scanner;

public class Menu {
    public static void showMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append("=================================================================================================\n");
        sb.append("|                                       HAPPY FAMILY                                            |\n");
        sb.append("=================================================================================================\n");
        sb.append("| 1. Fill with test data                                                                        |\n");
        sb.append("| 2. Display the entire list of families                                                        |\n");
        sb.append("| 3. Display a list of families where the number of people is greater than the specified number |\n");
        sb.append("| 4. Display a list of families where the number of people is less than the specified number    |\n");
        sb.append("| 5. Calculate the number of families where the number of members is                            |\n");
        sb.append("| 6. Create a new family                                                                        |\n");
        sb.append("| 7. Delete a family by its index in the general list                                           |\n");
        sb.append("| 8. Edit a family by its index in the general list                                             |\n");
        sb.append("| 9. Remove all children over the age of majority                                               |\n");
        sb.append("| 10. Exit                                                                                      |\n");
        sb.append("=================================================================================================\n");
        System.out.println(sb);
    }

    public static void editFamily() {
        StringBuilder sb = new StringBuilder();
        sb.append("==============================\n");
        sb.append("|        Edit family         |\n");
        sb.append("==============================\n");
        sb.append("| 1. Give a birth to a baby  |\n");
        sb.append("| 2. Adopt a child           |\n");
        sb.append("| 3. Return to main menu     |\n");
        sb.append("==============================\n");
        System.out.println(sb);
    }

    public static Man createFather() throws ParseException {
        UnixConsole console = new UnixConsole(new Scanner(System.in));
        console.printLn("Enter father's details");
        console.print("Name:");
        String name = console.readLn();
        console.print("Surname:");
        String surname = console.readLn();
        console.print("Birth Date (e.g. '01/01/1970'):");
        String birthDate = console.readLn();
        return new Man(name, surname, (byte) (Math.random()*50+50), birthDate);
    }

    public static Woman createMother() throws ParseException {
        UnixConsole console = new UnixConsole(new Scanner(System.in));
        console.printLn("Enter mother's details");
        console.print("Name:");
        String name = console.readLn();
        console.print("Surname:");
        String surname = console.readLn();
        console.print("Birth Date (e.g. '01/01/1970'):");
        String birthDate = console.readLn();
        return new Woman(name, surname, (byte) (Math.random()*50+50), birthDate);
    }

    public static Human createChild() throws ParseException {
        UnixConsole console = new UnixConsole(new Scanner(System.in));
        console.printLn("Enter child's details");
        console.printLn("Choose gender:\n" +
                "(boy or girl)");
        String gender = console.readLn();
        console.print("Name:");
        String name = console.readLn();
        console.print("Surname:");
        String surname = console.readLn();
        console.print("Birth Date (e.g. '01/01/1970'):");
        String birthDate = console.readLn();
        if (gender.equals("boy")){
            return new Man(name, surname, birthDate);
        }
        else {
            return new Woman(name, surname, birthDate);
        }

    }
}
