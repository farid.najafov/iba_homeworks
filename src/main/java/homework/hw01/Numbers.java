package homework.hw01;

import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        //noinspection InfiniteLoopStatement
        for (;;) {
            // start the game:
            System.out.println("Let the game begin!");
            System.out.println("Enter your name:");
            Scanner in = new Scanner(System.in);
            String name = in.nextLine();

            // random number generation
            Random rand = new Random();
            int randomNumber = rand.nextInt(101);

            System.out.println("Let's start!");
            System.out.println("Enter your number between 0 to 100:");

            int playerNumber;
            do {
                playerNumber = in.nextInt();

                if (playerNumber < randomNumber) {
                    System.out.println("Your number is too small. Please, try again.");
                } else if (playerNumber > randomNumber) {
                    System.out.println("Your number is too big. Please, try again.");
                }
            } while (playerNumber != randomNumber);

            System.out.printf("Congratulations, %s!\n", name);

        }
    }
}
