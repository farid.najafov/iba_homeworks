package homework.hw10.entity;

public enum Species {
    DomesticCat,
    Fish,
    Dog,
    RoboCat,
    UNKNOWN
}
