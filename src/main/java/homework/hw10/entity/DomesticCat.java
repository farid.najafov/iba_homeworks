package homework.hw10.entity;

import java.util.Set;

public class DomesticCat extends Pet {
    Species species = Species.DomesticCat;

    public DomesticCat(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    public DomesticCat(String nickname) {
        super(nickname);
    }

    void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", getNickname());
    }
    @Override
    public String toString() {
        return "DomesticCat{" +
                "species='" + species + '\'' +
                ", nickname='" + super.getNickname() + '\'' +
                ", age=" + super.getAge() +
                ", trickLevel=" + super.getTrickLevel() +
                ", habits=" + super.getHabits() +
                '}';
    }
}
