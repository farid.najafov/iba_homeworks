package homework.hw10.entity;


import java.text.ParseException;
import java.util.Map;

public final class Woman extends Human {
    public Woman(String name, String surname, byte iq, String birthDate) throws ParseException {
        super(name, surname, iq, birthDate);
    }

    public Woman(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, byte iq, Map<DayOfWeek, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, schedule);
    }

    public Woman() {
    }

    void makeUp() {
        System.out.println("I'm wearing makeup");
    }

    @Override
    public String toString() {
         return super.toString();
    }
}
