package homework.hw10.entity;

import java.text.ParseException;
import java.util.Map;

public final class Man extends Human {
    public Man(String name, String surname, byte iq, String birthDate) throws ParseException {
        super(name, surname, iq, birthDate);
    }

    public Man(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, byte iq, Map<DayOfWeek, String> schedule) throws ParseException {
        super(name, surname, birthDate, iq, schedule);
    }

    public Man() {
    }

    void repairCar() {
        System.out.println("I'm fixing my car");
    }

    @Override
    public String toString() {
         return super.toString();
    }
}
