package homework.hw10.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pet;

    //  constructors
    public Family() {

    }
    public Family(Human mother, Human father, List<Human> children, Set<Pet> pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    public Family(Human father, Human mother) {
        this.father = father;
        this.mother = mother;
    }

    //  methods
    void addChild (Human child) {
        if (children != null) {
            children.add(child);
        } else {
            List<Human> children = new ArrayList<>();
            children.add(child);
            setChildren(children);
        }
    }
    boolean deleteChild (Human child) {
        if (children == null) throw new IllegalArgumentException("this family has no child");
        if (!children.contains(child)) {
            System.out.println("no such child found in the family");
            return false;
        } else children.remove(child);
        return true;
    }
    public int countFamily() {
        return children.size() + 2;
    }

    //  getters and setters
    public Human getMother() {
        return mother;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }
    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }
    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }
    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Objects.equals(children, family.children);
    }
    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }
}
