package homework.hw10.Controller;

import homework.hw10.Service.FamilyService;
import homework.hw10.entity.Family;
import homework.hw10.entity.Human;
import homework.hw10.entity.Pet;

import java.util.List;
import java.util.Set;

public class FamilyController {
    FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int numOfMembers) {
        return familyService.getFamiliesBiggerThan(numOfMembers);
    }

    public List<Family> getFamiliesLessThan(int numOfMembers) {
        return familyService.getFamiliesLessThan(numOfMembers);
    }

    public int countFamiliesWithMemberNumber(int numOfMembers) {
        return familyService.countFamiliesWithMemberNumber(numOfMembers);
    }

    public void createNewFamily(Human father, Human mother) {
        familyService.createNewFamily(father, mother);
    }

    public boolean deleteFamilyByIndex(int familyIndex) {
        return familyService.deleteFamilyByIndex(familyIndex);
    }

    public boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public Family bornChild(Family family, String maleName, String femaleName) {
        return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThan(int age) {
        familyService.deleteAllChildrenOlderThan(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int familyId) {
        return familyService.getFamilyById(familyId);
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }

}
