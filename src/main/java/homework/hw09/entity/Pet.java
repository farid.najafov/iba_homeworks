package homework.hw09.entity;

import java.util.Set;

abstract public class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private byte trickLevel;
    private Set<String> habits;

    //  constructors
    public Pet(String nickname, int age, byte trickLevel, Set<String> habits){
        this.nickname = nickname;
        this.age = age;

        if (trickLevel >= 1 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else {
            throw new IllegalArgumentException("trickLevel must be a whole number from 1 to 100!");
        }

        this.habits = habits;
    }
    public Pet(String nickname) {
        this.nickname = nickname;
    }

    //  methods
    void eat() {
        System.out.println("I am eating");
    }
    abstract void respond();

    //  getters and setters
    public Species getSpecies() {
        return species;
    }
    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(byte trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }
    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

}
