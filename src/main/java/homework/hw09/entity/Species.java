package homework.hw09.entity;

public enum Species {
    DomesticCat,
    Fish,
    Dog,
    RoboCat,
    UNKNOWN
}
