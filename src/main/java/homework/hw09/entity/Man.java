package homework.hw09.entity;

import java.util.Map;

public final class Man extends Human {

    public Man(String name, String surname, int yearOfBirth, byte iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, yearOfBirth, iq, schedule);
    }

    public Man(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    public Man() {

    }

    void repairCar() {
        System.out.println("I'm fixing my car");
    }

    @Override
    public String toString() {
         return super.toString();
    }
}
