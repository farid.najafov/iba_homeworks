package homework.hw09.entity;


import java.util.Map;

public final class Woman extends Human {
    public Woman(String name, String surname, int yearOfBirth, byte iq, Map<DayOfWeek, String> schedule) {
        super(name, surname, yearOfBirth, iq, schedule);
    }

    public Woman(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    public Woman() {

    }

    void makeUp() {
        System.out.println("I'm wearing makeup");
    }

    @Override
    public String toString() {
         return super.toString();
    }
}
