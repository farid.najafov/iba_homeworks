package homework.hw09.entity;

import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

public class Human {
    private String name;
    private String surname;
    private int year;
    private byte iq;
    private Map<DayOfWeek, String> schedule;
    private Family family;

    //  constructors
    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;
    }
    public Human(String name, String surname, int yearOfBirth, byte iq, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;

        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }

        this.schedule = schedule;
    }

    public Human() {

    }
    public int getAge(){
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - this.year;
    }

    //  getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }
    public void setIq(byte iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }
    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }
    public void setFamily(Family family) {
        this.family = family;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                iq == human.iq &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(schedule, human.schedule) &&
                Objects.equals(family, human.family);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, schedule, family);
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                ", family=" + family +
                '}';
    }
}

