package homework.hw09.entity;

import java.util.Set;

public class Fish extends Pet {
    Species species = Species.Fish;

    public Fish(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }
    public Fish(String nickname) {
        super(nickname);
    }

    @Override
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", getNickname());
    }
    @Override
    public String toString() {
        return "Fish{" +
                "species='" + species + '\'' +
                ", nickname='" + super.getNickname() + '\'' +
                ", age=" + super.getAge() +
                ", trickLevel=" + super.getTrickLevel() +
                ", habits=" + super.getHabits() +
                '}';
    }
}
