package homework.hw09.DAO;

import homework.hw09.entity.Family;

import java.util.List;

public interface FamilyDAO {

    List<Family> getAllFamilies();
    Family getFamilyByIndex (int familyIndex);
    boolean deleteFamily (int familyIndex);
    boolean deleteFamily (Family family);
    void saveFamily (Family family);

}
