package homework.hw08;

import java.util.Map;

public final class Man04 extends Human04 {

    public Man04(String name, String surname, int yearOfBirth, byte iq, Map<DayOfWeek04, String> schedule) {
        super(name, surname, yearOfBirth, iq, schedule);
    }
    public Man04(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    void repairCar() {
        System.out.println("I'm fixing my car");
    }

    @Override
    public String toString() {
         return super.toString();
    }
}
