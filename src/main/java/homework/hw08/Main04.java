package homework.hw08;

import java.util.*;

public class Main04 {
    public static void main(String[] args) {
        // create a cat
        Set<String> catHabits = new HashSet<>();
        catHabits.add("eat");
        catHabits.add("drink");
        DomesticCat04 myCat = new DomesticCat04("Kitty", 5, (byte) 15, catHabits );

        // create a dog
        Set<String> dogHabits = new HashSet<>();
        dogHabits.add("run");
        dogHabits.add("play");
        Dog04 myDog = new Dog04("Rock", 3, (byte) 25, dogHabits );

        // create father
        Human04 father = new Man04("John", "Travolta", 1950);

        // create mother
        Human04 mother = new Woman04("Jenny", "Oldstones", 1970);

        // create firstChild
        Map<DayOfWeek04, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek04.MONDAY, "go to school");
        schedule.put(DayOfWeek04.TUESDAY, "go to library");
        Human04 firstChild = new Man04("Leo", "Tyler", 2000, (byte) 90, schedule);

        // create secondChild
        Map<DayOfWeek04, String> schedule2 = new HashMap<>();
        schedule2.put(DayOfWeek04.MONDAY, "go to gym");
        schedule2.put(DayOfWeek04.TUESDAY, "go to shopping");
        Human04 secondChild = new Woman04("Kate", "Tyler", 2005, (byte) 90, schedule2);

        // create array of pets
        Set<Pet04> pets = new HashSet<>();
        pets.add(myCat);
        pets.add(myDog);

        // create array of children
        List<Human04> children = new ArrayList<>();
        children.add(firstChild);

        // create family
        Family04 family = new Family04(mother, father, children, pets);
        family.addChild(secondChild); // adds second child to the family
        family.deleteChild(firstChild); // deletes first child from the family
        System.out.println(family);
        System.out.println(family.countFamily());
    }
}
