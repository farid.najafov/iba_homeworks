package homework.hw08;

import java.util.*;

public class Family04 {
    private Human04 mother;
    private Human04 father;
    private List<Human04> children;
    private Set<Pet04> pet;

    //  constructors
    Family04() {

    }
    Family04(Human04 mother, Human04 father, List<Human04> children, Set<Pet04> pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    //  methods
    void addChild (Human04 child) {
        if (children != null) {
            children.add(child);
        } else {
            List<Human04> children = new ArrayList<>();
            children.add(child);
            setChildren(children);
        }
    }
    boolean deleteChild (Human04 child) {
        if (children == null) throw new IllegalArgumentException("this family has no child");
        if (!children.contains(child)) {
            System.out.println("no such child found in the family");
            return false;
        } else children.remove(child);
        return true;
    }
    int countFamily() {
        return children.size() + 2;
    }

    //  getters and setters
    public Human04 getMother() {
        return mother;
    }
    public void setMother(Human04 mother) {
        this.mother = mother;
    }

    public Human04 getFather() {
        return father;
    }
    public void setFather(Human04 father) {
        this.father = father;
    }

    public List<Human04> getChildren() {
        return children;
    }
    public void setChildren(List<Human04> children) {
        this.children = children;
    }

    public Set<Pet04> getPet() {
        return pet;
    }
    public void setPet(Set<Pet04> pet) {
        this.pet = pet;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family04 family04 = (Family04) o;
        return Objects.equals(mother, family04.mother) &&
                Objects.equals(father, family04.father) &&
                Objects.equals(children, family04.children);
    }
    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children, pet);
    }
    @Override
    public String toString() {
        return "Family04{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet +
                '}';
    }

}
