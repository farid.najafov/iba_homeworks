package homework.hw08;

import java.util.Set;

public class Fish04 extends Pet04 {
    Species04 species = Species04.Fish;

    public Fish04(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", getNickname());
    }
    @Override
    public String toString() {
        return "Fish{" +
                "species='" + species + '\'' +
                ", nickname='" + super.getNickname() + '\'' +
                ", age=" + super.getAge() +
                ", trickLevel=" + super.getTrickLevel() +
                ", habits=" + super.getHabits() +
                '}';
    }
}
