package homework.hw08;

import java.util.Map;
import java.util.Objects;

public class Human04 {
    private String name;
    private String surname;
    private int year;
    private byte iq;
    private Map<DayOfWeek04, String> schedule;
    private Family04 family;

    //  constructors
    public Human04(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;
    }
    public Human04(String name, String surname, int yearOfBirth, byte iq, Map<DayOfWeek04, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;

        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }

        this.schedule = schedule;
    }

    //  getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }
    public void setIq(byte iq) {
        this.iq = iq;
    }

    public Map<DayOfWeek04, String> getSchedule() {
        return schedule;
    }
    public void setSchedule(Map<DayOfWeek04, String> schedule) {
        this.schedule = schedule;
    }

    public Family04 getFamily() {
        return family;
    }
    public void setFamily(Family04 family) {
        this.family = family;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human04 human04 = (Human04) o;
        return year == human04.year &&
                iq == human04.iq &&
                Objects.equals(name, human04.name) &&
                Objects.equals(surname, human04.surname) &&
                Objects.equals(schedule, human04.schedule) &&
                Objects.equals(family, human04.family);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, iq, schedule, family);
    }

    @Override
    public String toString() {
        return "Human04{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                ", family=" + family +
                '}';
    }
}

