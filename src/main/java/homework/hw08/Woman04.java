package homework.hw08;


import java.util.Map;

public final class Woman04 extends Human04 {
    public Woman04(String name, String surname, int yearOfBirth, byte iq, Map<DayOfWeek04, String> schedule) {
        super(name, surname, yearOfBirth, iq, schedule);
    }
    public Woman04(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    void makeUp() {
        System.out.println("I'm wearing makeup");
    }

    @Override
    public String toString() {
         return super.toString();
    }
}
