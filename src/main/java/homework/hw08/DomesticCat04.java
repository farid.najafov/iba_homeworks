package homework.hw08;

import java.util.Set;

public class DomesticCat04 extends Pet04{
    Species04 species = Species04.DomesticCat;

    public DomesticCat04(String nickname, int age, byte trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", getNickname());
    }
    @Override
    public String toString() {
        return "DomesticCat{" +
                "species='" + species + '\'' +
                ", nickname='" + super.getNickname() + '\'' +
                ", age=" + super.getAge() +
                ", trickLevel=" + super.getTrickLevel() +
                ", habits=" + super.getHabits() +
                '}';
    }
}
