package homework.hw08;

public enum Species04 {
    DomesticCat,
    Fish,
    Dog,
    RoboCat,
    UNKNOWN
}
