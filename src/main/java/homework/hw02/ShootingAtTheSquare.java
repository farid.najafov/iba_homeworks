package homework.hw02;

import java.util.Random;
import java.util.Scanner;

public class ShootingAtTheSquare {
    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");

        // random number generation
        Random rand = new Random();
        int randIndexX = rand.nextInt(5) + 1;
        int randIndexY = rand.nextInt(5) + 1;

        int userIndexX, userIndexY;

        char [][] square = new char[6][6];
        String str = "012345";
        square[0] = str.toCharArray();
        for (int i = 1; i < square.length; i++){
            square[i][0] = str.charAt(i);
        }
        for (int row = 1; row < square.length; row++) {
            for (int col = 1; col < square.length; col++) {
                square[row][col] = '-';
            }
        }

        printArr(square);

        do {
            // user input
            System.out.println("Enter coordinate X");
            userIndexX = checkNumber();
            System.out.println("Enter coordinate Y");
            userIndexY = checkNumber();

            square[userIndexX][userIndexY] = '*';
            printArr(square);
        } while (userIndexX != randIndexX && userIndexY != randIndexY);

        System.out.println("You have won!");
        square[userIndexX][userIndexY] = 'X';
        printArr(square);
    }

    public static int checkNumber() {
        Scanner input = new Scanner(System.in);
        int a = input.nextInt();
        while (a > 5 || a < 0) {
            System.out.println("Number is out of range! Try again:");
            a = input.nextInt();
        }return a;
    }

    public static void printArr(char [][] square){
        for (char[] chars : square) {
            for (int col = 0; col < square.length; col++) {
                System.out.print(chars[col] + "|");
            }
            System.out.println();
        }
    }
}

