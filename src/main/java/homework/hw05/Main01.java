package homework.hw05;

public class Main01 {
    public static void main(String[] args) {

        // creating father
        Human01 father = new Human01();
        father.setName("Brad");
        father.setSurname("Pitt");
        father.setYear(1960);

        // creating mother
        Human01 mother = new Human01();
        mother.setName("Angelina");
        mother.setSurname("Jolie");
        mother.setYear(1970);

        // creating first child
        Human01 firstChild = new Human01();
        firstChild.setName("Maddox");
        firstChild.setSurname("Jolie-Pitt");
        firstChild.setYear(2000);
        firstChild.setIq((byte)90);
        firstChild.setSchedule(new String[][]{{"Monday", "go to school"}, {"Tuesday", "go to library"}});

        // creating pet
        Pet01 cat = new Pet01();
        cat.setSpecies("cat");
        cat.setNickname("Kitty");
        cat.setAge(5);
        cat.setTrickLevel((byte)75);
        cat.setHabits(new String[]{"eat", "drink", "sleep"});

        // creating family
        Family01 newFamily = new Family01();
        newFamily.setMother(mother);
        newFamily.setFather(father);
        Human01[] children = {firstChild};
        newFamily.setChildren(children);
        newFamily.setPet(cat);

        // creating second child
        Human01 secondChild = new Human01();
        secondChild.setName("Vivienne");
        secondChild.setSurname("Jolie-Pitt");
        secondChild.setYear(2000);
        secondChild.setIq((byte)90);
        secondChild.setSchedule(new String[][]{{"Monday", "go to swim"}, {"Tuesday", "go to play"}});

        // add second child to the family
//        newFamily.addChild(secondChild);
        newFamily.countFamily();
        // delete first child from the family and return true if deleted
        System.out.println(newFamily.deleteChild(secondChild));

        // count members of family
        newFamily.countFamily();

        // print the family
        System.out.println(newFamily);

        newFamily.describePet();


    }
}
