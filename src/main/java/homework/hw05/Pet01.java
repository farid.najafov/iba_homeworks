package homework.hw05;

import java.util.Arrays;
import java.util.Objects;

public class Pet01 {
    private String species;
    private String nickname;
    private int age;
    private byte trickLevel;
    private String[] habits;

    //  constructors
    public Pet01() {

    }
    public Pet01(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    public Pet01(String species, String nickname, int age, byte trickLevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;

        if (trickLevel >= 1 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else {
            throw new IllegalArgumentException("trickLevel must be a whole number from 1 to 100!");
        }

        this.habits = habits;
    }

    //  methods
    void eat() {
        System.out.println("I am eating");
    }
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", getNickname());
    }
    void foul() {
        System.out.println("I need to cover it up");
    }


    //  getters and setters
    public String getSpecies() {
        return species;
    }
    public void setSpecies(String species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public byte getTrickLevel() {
        return trickLevel;
    }
    public void setTrickLevel(byte trickLevel) {
        if (trickLevel >= 1 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else {
            throw new IllegalArgumentException("trickLevel must be a whole number from 1 to 100!");
        }
    }

    public String[] getHabits() {
        return habits;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    // overridden methods


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet01 pet01 = (Pet01) o;
        return age == pet01.age &&
                trickLevel == pet01.trickLevel &&
                species.equals(pet01.species) &&
                nickname.equals(pet01.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    @Override
    public String toString() {
        return "Pet01{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
