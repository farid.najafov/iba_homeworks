package homework.hw05;

import java.util.Arrays;
import java.util.Objects;

public class Family01 {
    private Human01 mother;
    private Human01 father;
    private Human01[] children;
    private Pet01 pet;

    //  constructors
    Family01() {

    }
    Family01(Human01 mother, Human01 father, Human01[] children, Pet01 pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    //    methods
    void greetPet() {
        System.out.printf("Hello, %s\n", pet.getNickname());
    }
    void describePet() {
        if (pet.getAge() < 50) {
            System.out.printf("I have a %s, he is %s years old, he is very sly\n", pet.getSpecies(), pet.getAge());
        } else {
            System.out.printf("I have a %s, he is %s years old, he is almost not sly\n", pet.getSpecies(), pet.getAge());
        }
    }
    void addChild (Human01 child) {
        if(child == null) throw new IllegalArgumentException("Child cannot be null");

        if(this.children == null) {
            this.children = new Human01[] {child};
        } else {
            Human01[] newArray = new Human01[this.children.length + 1];
            for (int i = 0; i < children.length; i++ ){
                newArray[i] = this.children[i];
            }
            newArray[newArray.length-1] = child;
            this.children = newArray;
        }
    }
    boolean deleteChild (Human01 child) {
        if(child == null) throw new IllegalArgumentException("Child cannot be null!");
        boolean isDeleted = false;
        for (int k = 0; k < children.length; k++) {
            if (children[k] == child) {
                Human01[] newChildren = new Human01[children.length - 1];
                int countChildren = 0;
                for (Human01 ch : children) {
                    if (ch != child) {
                        newChildren[countChildren++] = ch;
                    } else
                        isDeleted = true;
                }
                children = newChildren;
            }
        }
        return  isDeleted;
    }
    void countFamily () {
        int count = 2 + children.length;
        System.out.printf("This family consists of %d person \n", count);
    }

    //  getters and setters
    public Human01 getMother() {
        return mother;
    }
    public void setMother(Human01 mother) {
        this.mother = mother;
    }

    public Human01 getFather() {
        return father;
    }
    public void setFather(Human01 father) {
        this.father = father;
    }

    public Human01[] getChildren() {
        return children;
    }
    public void setChildren(Human01[] children) {
        this.children = children;
    }

    public Pet01 getPet() {
        return pet;
    }
    public void setPet(Pet01 pet) {
        this.pet = pet;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family01 family01 = (Family01) o;
        return Objects.equals(mother, family01.mother) &&
                Objects.equals(father, family01.father) &&
                Arrays.equals(children, family01.children) &&
                pet.getSpecies().equals(family01.pet.getSpecies()) &&
                pet.getNickname().equals(family01.pet.getNickname());
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
    @Override
    public String toString() {
        return "Family01{" +
                "mother=" + mother + "\n" +
                ", father=" + father + "\n" +
                ", children=" + Arrays.toString(children) + "\n" +
                ", pet=" + pet +
                '}';
    }
}