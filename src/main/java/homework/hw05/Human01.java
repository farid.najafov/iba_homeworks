package homework.hw05;

import java.util.Arrays;
import java.util.Objects;

public class Human01 {
    private String name;
    private String surname;
    private int year;
    private byte iq;
    private String[][] schedule;
    private Family01 family;

    //  constructors
    public Human01(){

    }
    public Human01(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;
    }
    public Human01(String name, String surname, int yearOfBirth, byte iq, Family01 family) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;

        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }

        this.family = family;
    }

    //  getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public byte getIq() {
        return iq;
    }
    public void setIq(byte iq) {
        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }
    }

    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family01 getFamily() {
        return family;
    }
    public void setFamily(Family01 family) {
        this.family = family;
    }

    // overridden methods
    @Override
    public boolean equals(Object o) {
        System.out.println("Called Equals");
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human01 human01 = (Human01) o;
        return year == human01.year &&
                iq == human01.iq &&
                name.equals(human01.name) &&
                surname.equals(human01.surname) &&
                Arrays.equals(schedule, human01.schedule) &&
                family.equals(human01.family);
    }
    @Override
    public int hashCode() {
        System.out.println("Called hashCode");
        int result = Objects.hash(name, surname, year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }
    @Override
    public String toString() {
        return "Human01{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                ", family=" + family +
                '}';
    }
}
