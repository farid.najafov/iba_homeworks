package homework.hw04;

import java.util.Arrays;
import java.util.Random;

class Human {
    String name;
    String surname;
    int year;
    byte iq;
    Pet pet;
    Human mother;
    Human father;
    String[][] schedule;

//    constructors
    public Human(){

    }
    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;
    }
    public Human(String name, String surname, int yearOfBirth, Human motherName, Human fatherName) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;
        this.mother = motherName;
        this.father = fatherName;
    }
    public Human(String name, String surname, int yearOfBirth, byte iq, Pet pet, Human motherName, Human fatherName) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;

        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }

        this.pet = pet;
        this.mother = motherName;
        this.father = fatherName;
    }

//    methods
    void greetPet() {
        System.out.printf("Hello, %s\n", pet.nickname);
    }
    boolean checkAge() {
        return pet.age < 50;
    }
    void describePet() {

        if (checkAge()) {
            System.out.printf("I have a %s, he is %s years old, he is very sly\n", pet.species, pet.age);
        } else {
            System.out.printf("I have a %s, he is %s years old, he is almost not sly\n", pet.species, pet.age);
        }

    }
    boolean feedPet(boolean isItFeedingTime){

        Random random = new Random();
        int n = random.nextInt(100);
        if(!isItFeedingTime){
            if(this.pet.trickLevel > n){
                System.out.printf("Hm... I will feed Jack's %s\n", pet.nickname);
            }else{
                System.out.printf("I think %s is not hungry.\n", pet.nickname);
            }
        }
        return isItFeedingTime;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", pet=" + pet +
                ", mother=" + mother.name +
                ", father=" + father.name +
                ", schedule=" + Arrays.toString(schedule) +
                '}';
    }
}
