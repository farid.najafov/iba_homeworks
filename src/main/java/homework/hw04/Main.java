package homework.hw04;

public class Main {
    public static void main(String[] args) {

        Pet dog = new Pet("dog", "Rock", 55, (byte) 50, new String[]{"eat", "drink", "sleep"});
        dog.respond();
        dog.foul();
        dog.eat();
        System.out.println(dog);

        Human father = new Human("Vito", "Karleone", 1950);
        Human mother = new Human("Jane", "Karleone", 1955);
        Human child = new Human("Michael", "Karleone", 1977, (byte) 90, dog, mother, father);
        child.describePet();
        child.greetPet();
        System.out.println(child);
        child.feedPet(false);
    }
}
