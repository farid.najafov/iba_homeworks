package homework.hw04;

import java.util.Arrays;

public class Pet {
    String species;
    String nickname;
    int age;
    byte trickLevel;
    String[] habits;

//    constructors
    Pet() {

    }
    Pet(String species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    Pet(String species, String nickname, int age, byte trickLevel, String[] habits){
        this.species = species;
        this.nickname = nickname;
        this.age = age;

        if (trickLevel >= 1 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else {
            throw new IllegalArgumentException("trickLevel must be a whole number from 1 to 100!");
        }

        this.habits = habits;
    }

//    methods
    void eat() {
        System.out.println("I am eating");
    }
    void respond() {
        System.out.printf("Hello, owner. I am -  %s. I miss you!\n", this.nickname);
    }
    void foul() {
        System.out.println("I need to cover it up");
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }
}
