package homework.hw03;

import java.util.Scanner;

public class WeekPlanner {
    public static void main(String[] args) {
        // declaring the array
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Monday";
        schedule[0][1] = "Go to the course";
        schedule[1][0] = "Tuesday";
        schedule[1][1] = "Do homework";
        schedule[2][0] = "Wednesday";
        schedule[2][1] = "Go the library";
        schedule[3][0] = "Thursday";
        schedule[3][1] = "Do laundry";
        schedule[4][0] = "Friday";
        schedule[4][1] = "Meet friends";
        schedule[5][0] = "Saturday";
        schedule[5][1] = "Go to the cinema";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "Watch a film";

        //
        while (true) {
            Scanner in = new Scanner(System.in);
            System.out.println("Please, input the day of the week:");
            String day = in.nextLine().toLowerCase().trim();
            if (day.equals("exit")) break;
            switch (day) {
                case "monday":
                    System.out.println(schedule[0][1]);
                    break;
                case "tuesday":
                    System.out.println(schedule[1][1]);
                    break;
                case "wednesday":
                    System.out.println(schedule[2][1]);
                    break;
                case "thursday":
                    System.out.println(schedule[3][1]);
                    break;
                case "friday":
                    System.out.println(schedule[4][1]);
                    break;
                case "saturday":
                    System.out.println(schedule[5][1]);
                    break;
                case "sunday":
                    System.out.println(schedule[6][1]);
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}