package homework.hw06;

import java.util.Arrays;
import java.util.Objects;

public class Human02 {
    private String name;
    private String surname;
    private int year;
    private byte iq;
    private String[][] schedule;
    private Family02 family;
    static int count = 0;

    //  constructors
    public Human02(){
        count ++;
    }
    public Human02(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;
    }
    public Human02(String name, String surname, int yearOfBirth, byte iq) {
        this.name = name;
        this.surname = surname;
        this.year = yearOfBirth;

        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }
    }

    //  getters and setters
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public byte getIq() {
        return iq;
    }
    public void setIq(byte iq) {
        if (iq > 0 && iq <=100) {
            this.iq = iq;
        } else {
            throw new IllegalArgumentException("IQ must be a whole number from 1 to 100");
        }
    }

    public String[][] getSchedule() {
        return schedule;
    }
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family02 getFamily() {
        return family;
    }
    public void setFamily(Family02 family) {
        this.family = family;
    }

    //  overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human02 human02 = (Human02) o;
        return year == human02.year &&
                iq == human02.iq &&
                Objects.equals(name, human02.name) &&
                Objects.equals(surname, human02.surname) &&
                Arrays.equals(schedule, human02.schedule) &&
                Objects.equals(family, human02.family);
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq, family);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }
    @Override
    public String toString() {
        return "Human02{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                ", family=" + family +
                '}';
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleted");
    }
}
