package homework.hw06;

public class Main02 {
    public static void main(String[] args) {

//        for (int i = 0; i < 10_000; i++) {
//            Human02 human = new Human02();
//        }
//        System.gc();
//        int count = Human02.count;
//        System.out.println(count);

        DayOfWeek mon = DayOfWeek.MONDAY;
        DayOfWeek tue = DayOfWeek.TUESDAY;
        DayOfWeek wen = DayOfWeek.WEDNESDAY;
        DayOfWeek thu = DayOfWeek.THURSDAY;
        DayOfWeek fri = DayOfWeek.FRIDAY;
        DayOfWeek sat = DayOfWeek.SATURDAY;
        DayOfWeek sun = DayOfWeek.SUNDAY;

        // create child
        Human02 child = new Human02();
        child.setName("Vivienne");
        child.setSurname("Jolie-Pitt");
        child.setYear(2000);
        child.setIq((byte)90);
        child.setSchedule(new String[][]{{mon.name(), "go to swim"}, {tue.name(), "go to play"}, {wen.name(), "go to hell"}});

        System.out.println(child);

    }
}
