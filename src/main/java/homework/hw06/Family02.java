package homework.hw06;

import java.util.Arrays;
import java.util.Objects;

public class Family02 {
    private Human02 mother;
    private Human02 father;
    private Human02[] children;
    private Pet02 pet;
    public Human02[] newArray;

    //  constructors
    Family02() {

    }
    Family02(Human02 mother, Human02 father, Human02[] children, Pet02 pet) {
        this.mother = mother;
        this.father = father;
        this.children = children;
        this.pet = pet;
    }

    //  methods
    void greetPet() {
        System.out.printf("Hello, %s\n", pet.getNickname());
    }
    void describePet() {
        if (pet.getAge() < 50) {
            System.out.printf("I have a %s, he is %s years old, he is very sly\n", pet.getSpecies(), pet.getAge());
        } else {
            System.out.printf("I have a %s, he is %s years old, he is almost not sly\n", pet.getSpecies(), pet.getAge());
        }
    }
    void addChild (Human02 child) {
        if(child == null) throw new IllegalArgumentException("Child cannot be null");

        if(this.children == null) {
            this.children = new Human02[] {child};
        } else {
            newArray = new Human02[this.children.length + 1];
            for (int i = 0; i < children.length; i++ ){
                newArray[i] = this.children[i];
            }
            newArray[newArray.length-1] = child;
            this.children = newArray;
        }
    }
    boolean deleteChild (Human02 child) {
        if(child == null) throw new IllegalArgumentException("Child cannot be null!");
        boolean isDeleted = false;
        for (int k = 0; k < children.length; k++) {
            if (children[k] == child) {
                Human02[] newChildren = new Human02[children.length - 1];
                int countChildren = 0;
                for (Human02 ch : children) {
                    if (ch != child) {
                        newChildren[countChildren++] = ch;
                    } else
                        isDeleted = true;
                }
                children = newChildren;
            }
        }
        return  isDeleted;
    }
    int countFamily () {
        return 2 + children.length;
    }

    //  getters and setters
    public Human02 getMother() {
        return mother;
    }
    public void setMother(Human02 mother) {
        this.mother = mother;
    }

    public Human02 getFather() {
        return father;
    }
    public void setFather(Human02 father) {
        this.father = father;
    }

    public Human02[] getChildren() {
        return children;
    }
    public void setChildren(Human02[] children) {
        this.children = children;
    }

    public Pet02 getPet() {
        return pet;
    }
    public void setPet(Pet02 pet) {
        this.pet = pet;
    }

    //  overridden methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family02 family02 = (Family02) o;
        return Objects.equals(mother, family02.mother) &&
                Objects.equals(father, family02.father) &&
                Arrays.equals(children, family02.children) &&
                pet.getSpecies().equals(family02.pet.getSpecies()) &&
                pet.getNickname().equals(family02.pet.getNickname());
    }
    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }
    @Override
    public String toString() {
        return "Family01{" +
                "mother=" + mother + "\n" +
                ", father=" + father + "\n" +
                ", children=" + Arrays.toString(children) + "\n" +
                ", pet=" + pet +
                '}';
    }
}