package classwork.lesson06;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

    Cat myCat = new Cat("Kitty");
    Dog myDog = new Dog("Doggy");
    Fish myFish = new Fish("Fishy");

    Animal dragon = new Animal("Drogo") {
        @Override
        public String toString() {
            return "Hello, I am a dragon, my name is \n" + name;
        }
    };

    ArrayList<Animal> animals = new ArrayList<>();
    animals.add(myCat);
    animals.add(myDog);
    animals.add(myFish);
    animals.add(dragon);

    System.out.print(animals);

    }
}
