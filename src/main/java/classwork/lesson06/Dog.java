package classwork.lesson06;

public class Dog extends  Animal {

    Dog(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Hello, I am a dog, my name is " + name;
    }
}
