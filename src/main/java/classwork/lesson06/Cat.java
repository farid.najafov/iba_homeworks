package classwork.lesson06;

public class Cat extends Animal {
    Cat(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return "Hello, I am a cat, my name is " + name;
    }
}
