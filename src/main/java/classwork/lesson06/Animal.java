package classwork.lesson06;

public class Animal {
    String name;

    Animal (String name) {
        this.name = "\"" + name.toUpperCase() + "\"";
    }

    @Override
    public String toString() {
        return "Hello, I am an animal, my name is " + name;
    }
}
