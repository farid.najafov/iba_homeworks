package classwork.Lesson20bitwise_ops;

public class InvertString {
    public static boolean isCapital(char c) {
        return c >= 'A' && c <= 'Z';
    }
    String invert(String origin) {
        StringBuilder sb = new StringBuilder(origin);
        for (int i = 0; i < origin.length(); i++) {
            if (isCapital(origin.charAt(i))) {
                sb.replace(i, i+1, origin.substring(i, i+1).toLowerCase());
            } else sb.replace(i, i+1, origin.substring(i, i+1).toUpperCase());
        }
        return new String(sb);
    }
    String invert2(String origin) {
        char[] chars = origin.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char)(chars[i] ^ 0b100000);
        }
        return new String(chars);
    }

    String toUpperCase(String origin) {
        char[] chars = origin.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            int bit  =         0b00100000;
            int mask = ~bit; //0b11011111
            chars[i] = (char)(chars[i] & mask); // switch OFF bit &&
        }
        return new String(chars);
    }
    String toLowerCase(String origin) {
        char[] chars = origin.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            int bit = 0b100000;
            chars[i] = (char)(chars[i] | bit); // switch ON bit
        }
        return new String(chars);
    }

    public static void main(String[] args) {
        InvertString is = new InvertString();
        System.out.println(is.invert("abC"));
        System.out.println(is.invert2("abC"));
        System.out.println(is.toLowerCase("abC"));
        System.out.println(is.toUpperCase("abC"));
    }
}
