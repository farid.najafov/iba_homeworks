package classwork.Lesson20bitwise_ops;

public class BinaryToInteger {
  public static void main(String[] args) {
    String binary = "00010010"; // 18
    int result = 0;
    for (int i = binary.length()-1; i >= 0 ; i--) {
      char ch = binary.charAt(i); // '0' or '1' 48 or 49
      int digit = ch - '0';       // 0 or 1
//      int delta = (int) Math.pow(digit, binary.length() -i -1);
      int delta = digit << (binary.length() -i -1);
      result += delta;
    }
    System.out.println(result); // 18
  }
}
