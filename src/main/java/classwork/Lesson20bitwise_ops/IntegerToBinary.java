package classwork.Lesson20bitwise_ops;

public class IntegerToBinary {
    static void integerToBinaryV1(int i) {
        StringBuilder binary = new StringBuilder();
        while (i > 0) {
            int rem = i % 2;
            binary.append(rem).append(" ");
            i = i / 2;
        }
        System.out.println(binary.reverse().toString());
    }
    static void integerToBinaryV2(int i) {
        StringBuilder binary = new StringBuilder();
        while (i > 0) {
            int rem = i & 0b1;
            binary.append(rem).append(" ");
            i = i >> 1;
        }
        System.out.println(binary.reverse().toString());
    }
    static void integerToBinaryV3(int value) {
        StringBuilder binary = new StringBuilder();
        for (int i = 7; i >= 0 ; i--) {
            int part = value >> i;
            int bit = part & 0b00000001;
            binary.append(bit).append(" ");
        }
        System.out.println(binary.toString()); // 00010010
    }
    public static void main(String[] args) {
        integerToBinaryV1(255);
        integerToBinaryV2(255);
        integerToBinaryV3(255);

    }
}
