package classwork.lesson28SnakeChallenge;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Parking {
  public static long carParkingRoof(List<Long> cars, int k) {
    long minLen = Long.MAX_VALUE;
    Collections.sort(cars);
    for (int i = 0; i < cars.size() - (k - 1); i++) {
       if (cars.get(i + k - 1) - cars.get(i) < minLen) {
         minLen = (cars.get(i + k - 1) - cars.get(i) + 1);
       }
    }
    return minLen;
  }

  public static void main(String[] args) {
    List<Long> cars = Arrays.asList(6L, 2L, 12L, 7L);
    long r = carParkingRoof(cars, 3);
    System.out.println(r); // 6
  }
}
