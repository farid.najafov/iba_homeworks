package classwork.lesson01;

import java.util.Scanner;

public class CalcApp {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number a:");
        int a = in.nextInt();
        System.out.print("Enter the number b:");
        int b = in.nextInt();

        int c = a + b;
        System.out.printf("The sum is: %d\n", c);

    }
}
