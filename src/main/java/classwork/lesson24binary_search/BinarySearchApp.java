package classwork.lesson24binary_search;

import java.util.Arrays;
import java.util.Random;

public class BinarySearchApp {
  static int[] data = new Random().ints(0, 10000).limit(1000).toArray();
  static int[] sorted = data.clone();

  // count number of `if` statements
  // executed to find the given element
  static int find_conventional(int[] origin, int number) {
    int count = 0;
    for (int value : origin) {
      count++;
      if (number == value) break;
    }
    return count;
  }

  // count number of `if` statements
  // executed to find the given element
  static int find_binary(int[] origin, int number) {
    int count = 0;
    int l = 0;
    int r = origin.length - 1;

    while (l < r) {
      count++;
      int m = (l + r) / 2;
      if (number == origin[m]) break;
      else if (number > origin[m]) l = m + 1;
      else r = m - 1;
    }
    return count;
  }

  public static void main(String[] args) {
    Arrays.sort(sorted);
//    System.out.println(Arrays.toString(data));
//    System.out.println(Arrays.toString(sorted));
    int rnd = (int) (Math.random()*10000);
    int count1 = find_conventional(data, rnd);
    int count2 = find_binary(sorted, rnd);
    System.out.println(count1);
    System.out.println(count2);
  }
}
