package classwork.lesson02;

import java.util.Scanner;

public class WarmUpApp {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Hello, what is your name?");
        String name = in.nextLine();
        System.out.printf("Hello, %s!\n", name);
        System.out.printf("Nice to meet you, %s!\n", name);
        System.out.println("How old are you?");
        int age = in.nextInt();

        if (age < 18) {
            System.out.println("Let's go to the cinema!");
        } else {
            System.out.println("Let's go to the night club!");
        }


    }
}
