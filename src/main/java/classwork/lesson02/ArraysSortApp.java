package classwork.lesson02;

import java.util.Arrays;

public class ArraysSortApp {
    public static void main(String[] args) {
        int[] numbers = new int[30];

        for (int i = 0; i < numbers.length; i++) {
            int rand = (int)(Math.random()*200-100);
            numbers[i] = rand;
        }

        System.out.println(Arrays.toString(numbers));

        int count_pe = 0;
        int count_po = 0;
        int count_ne = 0;
        int count_no = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < 0) {
                if (numbers[i] % 2 == 0) {
                    count_ne++;
                } else {
                    count_no++;
                }
            } else if (numbers[i] % 2 == 0) {
                count_pe++;
            } else {
                count_po++;
            }
        }

        int[] ne = new int[count_ne];
        int[] no = new int[count_no];
        int[] pe = new int[count_pe];
        int[] po = new int[count_po];

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < 0) {
                if (numbers[i] % 2 == 0) {
                    ne[ne.length - count_ne--] = numbers[i];
                } else {
                    no[no.length - count_no--] = numbers[i];
                }
            } else if (numbers[i] % 2 == 0) {
                pe[pe.length - count_pe--] = numbers[i];
            } else {
                po[po.length - count_po--] = numbers[i];
            }
        }

        System.out.println(Arrays.toString(ne));
        System.out.println(Arrays.toString(no));
        System.out.println(Arrays.toString(pe));
        System.out.println(Arrays.toString(po));
    }
}
