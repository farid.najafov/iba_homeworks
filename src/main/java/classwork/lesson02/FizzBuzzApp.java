package classwork.lesson02;

public class FizzBuzzApp {
    public static void main(String[] args) {
        for (int i = 1; i <= 30; i++) {
            if (i % 2 == 0 && i % 3 == 0) {
                System.out.println("fizzbuzz");
            } else if (i % 2 == 0) {
                System.out.println("fizz");
            } else if (i % 3 == 0) {
                System.out.println("buzz");
            } else {
                System.out.println(i);
            }
        }
    }
}
