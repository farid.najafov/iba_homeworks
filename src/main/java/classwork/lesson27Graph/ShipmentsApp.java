package classwork.lesson27Graph;

import java.util.ArrayList;
import java.util.List;

public class ShipmentsApp {

  static int fix(int[] w) {
    int moves = 0;
    int sum = 0;
    List<Integer> list = new ArrayList<>();

    for (int value : w) {
      sum += value;
    }

    if (sum % w.length != 0) { return -1; }
    else {
      for (int value : w) {
        int s = value - sum / w.length;
        if (s > 0) {
          list.add(s);
        }
      }
    }

    for (Integer integer : list) {
      moves += integer;
    }
    return moves;
  }

  public static void main(String[] args) {
    int[] a1 = {1,1,1,1,6};
    int[] a2 = {1,1,1,1,16};
    int[] a3 = {1,1,1,1,15};
    int[] a4 = {10,20,2,3,15};
    System.out.println(fix(a1)); // 4
    System.out.println(fix(a2)); // 12
    System.out.println(fix(a3)); // -1
    System.out.println(fix(a4)); // 15
  }
}
