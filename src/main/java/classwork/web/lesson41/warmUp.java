package classwork.web.lesson41;

import java.math.BigInteger;

public class warmUp {
    static void extraLongFactorials(int n) {
        BigInteger res = new BigInteger("1");
        for (int i = 2; i <= n; i++)
            res = res.multiply(BigInteger.valueOf(i));
        System.out.println(res);
    }

    public static void main(String[] args) {
        extraLongFactorials(30);
    }
}
