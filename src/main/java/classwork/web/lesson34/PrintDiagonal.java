package classwork.web.lesson34;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PrintDiagonal {

    static class Track {
        int r = 0;
        int c = 0;
        int d = 0;
    }

    public static String dataOrdered(int R, int C, int[][] m) {
        Track t = new Track();
        return IntStream.range(0, R*C).map(n -> {
            int val = m[t.r][t.c];
            switch (t.d) {
                case 0: t.c++; t.d = 1; break;
                case 1:
                    if      (t.r == R - 1) { t.c++; t.d = 2; }
                    else if (t.c == 0)     { t.r++; t.d = 2; }
                    else                   { t.r++; t.c--; }
                    break;
                case 2:
                    if      (t.c == C - 1) { t.r++; t.d = 1; }
                    else if (t.r == 0)     { t.c++; t.d = 1; }
                    else                   { t.r--; t.c++; }
                    break;
            }
            return val;
        })
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(" "));
    }
    public static void main(String[] args) {
        int[][]a =
                {
                        {  1,  2,  3, 4 },
                        {  5,  6,  7, 8 },
                        {  9, 10, 11, 12 },
                        { 13, 14, 15, 16 },
                        { 17, 18, 19, 20 },
                        { 21, 22, 23, 24 },
                        { 25, 26, 27, 28 },
                };
        System.out.println(dataOrdered(a.length, a[0].length, a));
    }
}
