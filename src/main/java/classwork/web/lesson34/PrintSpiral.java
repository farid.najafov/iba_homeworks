package classwork.web.lesson34;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;

public class PrintSpiral {
    static class Track {
        int row = 0;
        int col = 0;
        int dir = 0;
        int arm = 0;
    }

    public static String dataOrdered(int R, int C, int[][] m) {
        Track t = new Track();
        return IntStream.range(0, R*C).map(x -> {
            int val = m[t.row][t.col];
            switch (t.dir) {
                case 0:
                    if (t.col < C - 1 - t.arm) { t.col++; }
                    else                       { t.row++;  t.dir = 1; }
                    break;
                case 1:
                    if (t.row < R - 1 - t.arm) { t.row++; }
                    else                       { t.dir = 2; t.col--; }
                    break;
                case 2:
                    if (t.col > t.arm)         { t.row = R - 1; t.col--; }
                    else                       { t.dir = 3; t.row--; t.arm++; }
                    break;
                case 3:
                    if (t.row > t.arm)         { t.row--; }
                    else                       { t.col++; t.dir = 0; }
            }
            return val;
        })
                .mapToObj(String::valueOf)
                .collect(joining(" "));
    }


    public static void main(String[] args) {
        int[][]a =
                {
                        {  1,  2,  3, 4 },
                        {  5,  6,  7, 8 },
                        {  9, 10, 11, 12 },
                        { 13, 14, 15, 16 },
                        { 17, 18, 19, 20 },
                        { 21, 22, 23, 24 },
                        { 25, 26, 27, 28 },
                };
        System.out.println("1 2 3 4 8 12 16 20 24 28 27 26 25 21 17 13 9 5 6 7 11 15 19 23 22 18 14 10");
        System.out.println(dataOrdered(a.length, a[0].length, a));
    }
}
