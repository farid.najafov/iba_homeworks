package classwork.web.lesson39;

import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.function.Predicate;

public class ApplesAndOranges {
    static void countApplesAndOranges(int s, int t, int a, int b, int[] apples, int[] oranges) {
        IntPredicate predicate = x -> x >= s && x <= t;
        long count = Arrays.stream(apples).map(x -> x + a).filter(predicate).count();
        long count2 = Arrays.stream(oranges).map(x -> x + b).filter(predicate).count();

        System.out.println(count);
        System.out.println(count2);
        System.out.printf("%d %d", count,count2 );

    }

    public static void main(String[] args) {
        int s = 7;
        int t = 11;
        int a = 5;
        int b = 15;
        int[] apples = {-2, 2, 1};
        int[] oranges = {5, -6};
        countApplesAndOranges(s, t, a, b, apples, oranges);
    }
}
