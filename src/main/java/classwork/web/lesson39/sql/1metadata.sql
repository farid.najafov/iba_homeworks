create table if not exists users
(
	id serial not null constraint users_pk primary key,
	age integer,
	name varchar
);

create unique index if not exists users_id_uindex on users (id);

create table if not exists "group"
(
	id serial not null constraint group_pk primary key,
	name varchar
);

create unique index if not exists group_id_uindex on "group" (id);

