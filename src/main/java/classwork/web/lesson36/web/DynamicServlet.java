package classwork.web.lesson36.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;


public class DynamicServlet extends HttpServlet {
    private final TemplateEngine engine;

    public DynamicServlet(TemplateEngine engine) {
        this.engine = engine;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp){
        String name = "Jim";
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", name);

        engine.render("dynamic1.ftl", data, resp);
    }
}
