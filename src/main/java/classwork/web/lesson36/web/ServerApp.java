package classwork.web.lesson36.web;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class ServerApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();

        TemplateEngine engine = TemplateEngine.folder("/content");
        handler.addServlet(new ServletHolder(new DynamicServlet(engine)), "/*");
        handler.addServlet(new ServletHolder(new DynamicServlet2(engine)), "/students/*");

        server.setHandler(handler);
        server.start();
        server.join();
    }
}
