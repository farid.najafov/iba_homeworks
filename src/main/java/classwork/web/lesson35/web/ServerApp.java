package classwork.web.lesson35.web;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class ServerApp {
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler();

        handler.addServlet(new ServletHolder(new StaticServlet()), "/images/*");
        handler.addServlet(new ServletHolder(new LoginServlet()), "/login/*");

        handler.addServlet(new ServletHolder(new RedirectServlet("/login")), "/*");


        server.setHandler(handler);
        server.start();
        server.join();
    }
}
