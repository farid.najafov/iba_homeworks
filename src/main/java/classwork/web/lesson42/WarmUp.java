package classwork.web.lesson42;

import java.util.*;


public class WarmUp {
    static int[] climbingLeaderboard(int[] scores, int[] alice) {
        int[] result = new int[alice.length];

        Integer[] sc = Arrays.stream(scores).boxed().distinct().toArray(Integer[]::new);

        for (int i = 0; i < alice.length; i++) {
            int x = Arrays.binarySearch(sc, alice[i], Collections.reverseOrder());
            x = x < 0 ? -x : x + 1;
            result[i] = x;
        }
        return result;
    }
    public static void main(String[] args) {
        int[] scores = {100, 90, 90, 80, 75, 60};
        int[] alice = {50, 65, 77, 90, 102};
        System.out.println(Arrays.toString(climbingLeaderboard(scores, alice)));
    }

}
