package classwork.web.lesson38;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class GradingStudents {
    public static List<Integer> gradingStudents(List<Integer> grades) {
        return grades.stream().map(
                x -> (x >= 38 && x % 5 >= 3) ? x + 5 - x % 5 : x
        ).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Integer> grades = new Random().ints(0, 100)
                .limit(new Random().nextInt(60)).boxed().collect(Collectors.toList());
        System.out.println(grades);
        System.out.println( gradingStudents(grades) );
    }
}
