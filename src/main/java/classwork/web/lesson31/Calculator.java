package classwork.web.lesson31;

public class Calculator {
  public String doOperation(String x, String y, String op) {
    int a = Integer.parseInt(x);
    int b = Integer.parseInt(y);
    int result = 0;
    switch (op) {
      case "add":
        result = a + b;
        break;
      case "mul":
        result = a * b;
        break;
      case "div":
        result = a / b;
        break;
      case "sub":
        result = a - b;
        break;
      default:
    }
    return String.valueOf(result);
  }
}
