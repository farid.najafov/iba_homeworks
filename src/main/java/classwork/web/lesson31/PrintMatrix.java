package classwork.web.lesson31;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PrintMatrix {
  public static String dataOrdered(int R, int C, int[][] m) {
    return IntStream.range(0, R*C).map(idx -> {
      int row = idx / C;
      int shift = idx - row * C;
      int col = (row&1)==0 ? shift : C-1-shift;
      return m[row][col];
    })
            .mapToObj(String::valueOf)
            .collect(Collectors.joining(" "));
  }
  public static String dataOrdered2(int R, int C, int[][] m) {
    return IntStream.range(0, R*C).map(idx -> {
      int row = idx / C;
      int shift = idx - row * C;
      int col = (row&1)==0 ? C-1-shift : shift;
      return m[row][col];
    })
            .mapToObj(String::valueOf)
            .collect(Collectors.joining(" "));
  }


  public static void main(String[] args) {
    int[][]a =
        {
        {  1,  2,  3 },
        {  5,  6,  7 },
        {  9, 10, 11 },
        { 13, 14, 15 },
        { 17, 18, 19 },
        { 21, 22, 23 },
    };
    System.out.println(dataOrdered(a.length, a[0].length, a));
    // 1 2 3 7 6 5 9 10 11 15 14 13 17 18 19 23 22 21

    System.out.println(dataOrdered2(a.length, a[0].length, a));
    // 3 2 1 5 6 7 11 10 9 13 14 15 19 18 17 21 22 23
  }
}
