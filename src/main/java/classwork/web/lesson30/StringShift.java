package classwork.web.lesson30;

public class StringShift {
  public static String shift(String orig, int toLeft, int toRight) {
    int len = orig.length();
    int total = (toRight - toLeft) % len;
    if (total == 0) return orig;

    int middle = (total > 0) ? len-total: -total;
    return orig.substring(middle)
            .concat(orig.substring(0, middle));
  }

  // works only when shift is less than string length
  public static String shift2(String orig, int toLeft, int toRight) {
    int shift = Math.abs(toLeft - toRight);
    if (toLeft > toRight) return leftRotate(orig, shift);
    else return leftRotate(orig, orig.length() - shift);
  }

  private static String leftRotate(String orig, int toLeft) {
    return orig.substring(toLeft) + orig.substring(0, toLeft);
  }

  public static void main(String[] args) {
    String s = "abcd";
    System.out.println(shift(s, 2, 0));
    System.out.println(shift2(s, 2, 4));

  }
}
