package classwork.web.lesson47;

import org.apache.logging.log4j.LogManager;

import java.util.Arrays;
import java.util.stream.IntStream;

public class WarmUp {
    static int[] stones(int n, int a, int b) {
        int[] ints = new int[n];

        for (int i = 0, j = n - 1; i < n; i++, j--) {
            ints[i] = i * a + j * b;
        }

        return Arrays.stream(ints).distinct().sorted().toArray();

    }
    static int[] stonesV2(int n, int a, int b) {
        return IntStream.range(0, n).map(x -> x * a + (n - 1 - x) * b).distinct().sorted().toArray();


    }
}
