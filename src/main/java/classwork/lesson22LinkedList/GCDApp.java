package classwork.lesson22LinkedList;

public class GCDApp {
  int gcd(int a, int b) {
    int min = Math.min(a, b);
    int max = Math.max(a, b);
    int r = max % min;
    if (r == 0) return min;
    return gcd(min, r);
  }
}
