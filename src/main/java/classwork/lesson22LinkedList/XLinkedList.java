package classwork.lesson22LinkedList;

import java.util.StringJoiner;

public class XLinkedList {
    static class Node {
        final int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
        Node (int value, Node next) {
            this.value = value;
            this.next = next;
        }
    }
    Node head;

    void prepend(int element) {
        Node node = new Node(element);
        node.next = head;
        head = node;
    }
    //    void prepend2(int element) {
//        head = new Node(element).next = head;
//    }
    void prepend3(int element) {
        head = new Node(element, head);
    }

    void append(int element) {
        Node node = new Node(element);
        if (head == null) {
            head = node;
        } else {
            Node curr = head;
            while (curr.next != null) {
                curr = curr.next;
            }
            curr.next = node;
        }
    }

    boolean contains_iter(int element) {
        Node curr = head;
        while (curr != null) {
            if (curr.value == element) return true;
            curr = curr.next;
        }
        return false;
    }

    boolean containsFrom(Node curr, int element) {
        if (curr == null) return false;
        if (curr.value == element) return true;
        return containsFrom(curr.next, element);
    }

    boolean contains(int element) {
        return containsFrom(head, element);
    }

    void insertBefore(int index, int value) {
        insertAfter(index - 1, value);
    }

    void insertBeforeValue(int valueLookFor, int value) {
        Node newNode = new Node(value);
        Node curr = head;
        Node prev = null;
        while (curr.value != valueLookFor) {
            prev = curr;
            curr = curr.next;
        }
        newNode.next = curr;
        prev.next = newNode;
    }

    void insertBeforeValue2(int valueLookFor, int value) {
        Node node = new Node(value);
        Node curr = head;
        while (curr.next.value != valueLookFor) {
            curr = curr.next;
        }
        node.next = curr.next;
        curr.next = node;
    }

    void insertAfter(int index, int value) {
        Node node = new Node(value);
        Node curr = head;
        while (index > 1) {
            curr = curr.next;
            index--;
        }
        node.next = curr.next;
        curr.next = node;
    }

    void insertAfter_for(int index, int value) {
        Node node = new Node(value);
        Node curr = head;
        for (; index > 1; curr = curr.next, index--) {}
        node.next = curr.next;
        curr.next = node;
    }

    void delete(int value) {
        Node curr = head;
        Node prev = null;
        while (curr != null) {
            if (curr.value == value) {
                if (prev == null) {
                    head = head.next;
                    break;
                }
                prev.next = curr.next;
                break;
            }
            prev = curr;
            curr = curr.next;
        }
    }

    void deleteLast() {
        throw new IllegalArgumentException("deleteLast:hasn't implemented yet");
    }

    void deleteAt(int index) {
        throw new IllegalArgumentException("deleteAt:hasn't implemented yet");
    }

    public void reverse() {
        Node curr = head;
        Node prev = null;
        while (curr != null) {
            Node savedNext = curr.next; // 1 - save
            curr.next = prev;           // 2 - our core operation
            prev = curr;                // 3 - move prev pointer to the next step
            curr = savedNext;           // 3 - move curr pointer to the next step
        }
        head = prev;
    }

    private Node reverser(Node curr, Node prev) {
        if (curr == null) return prev;
        Node savedNext = curr.next; // 1 - save
        curr.next = prev;           // 2 - our core operation
        return reverser(savedNext, curr);  // 3 - recursion, next step
    }

    public void reverser() {
        head = reverser(head, null);
    }

    int length_iter() {
        int len = 0;
        for (Node curr = head; curr != null; curr = curr.next) len++;
        return len;
    }

    int length_headRec(Node curr) {
        if (curr == null) return 0;
        int partialLen = length_headRec(curr.next);
        return partialLen + 1;
    }

    int length_headRec() {
        return length_headRec(head);
    }

    int length_tailRec(Node curr, int acc) {
        if (curr == null) return acc;
        return length_tailRec(curr.next, acc + 1);
    }

    int length_tailRec() {
        return length_tailRec(head,0);
    }


    String represent() {
        StringJoiner sj = new StringJoiner(",", "(", ")");
        Node curr = head;
        while (curr != null) {
            sj.add(String.valueOf(curr.value));
            curr = curr.next;
        }
        return sj.toString();
    }

    String represent2() {
        StringJoiner sj = new StringJoiner(",", "(", ")");
        for (Node curr = head; curr != null; curr = curr.next) {
            sj.add(String.valueOf(curr.value));
        }
        return sj.toString();
    }

    private void attach_next(Node curr, StringJoiner sj) {
        if (curr == null) return;
        attach_next(curr.next, sj.add(String.valueOf(curr.value)));
    }
    String representRec() {
        StringJoiner sj = new StringJoiner(",", "(", ")");
        attach_next(head, sj);
        return sj.toString();
    }
}
