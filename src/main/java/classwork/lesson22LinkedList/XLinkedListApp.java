package classwork.lesson22LinkedList;

public class XLinkedListApp {
    public static void main(String[] args) {
        XLinkedList xl = new XLinkedList();
        xl.prepend(1);
        xl.prepend(2);
        xl.prepend(3);
        xl.append(10);
        xl.append(20);
        System.out.println(xl.represent());
        System.out.println(xl.contains(15));
        xl.insertAfter(3, 5);
        xl.insertAfter_for(4,7);
        System.out.println(xl.representRec());
        xl.insertBefore(3, 15);
        System.out.println(xl.represent2());
        xl.insertBeforeValue(7, 55);
        System.out.println(xl.represent2());
        System.out.println(xl.length_iter());
        System.out.println(xl.length_headRec());
        System.out.println(xl.length_tailRec());
        xl.delete(20);
        System.out.println(xl.represent());


    }
}
