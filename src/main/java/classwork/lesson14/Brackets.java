package classwork.lesson14;

public class Brackets {
    public int calc(String origin) {
        int cur = 0;
        int max = 0;
        for (int i=0; i<origin.length(); i++){
//      cur += origin.charAt(i)=='(' ? 1 : -1;
            switch (origin.charAt(i)) {
                case '(': cur+=1; break;
                case ')': cur-=1; break;
                default : throw new IllegalArgumentException("wrong char");
            }
            max = Math.max(max, cur);
        }
        return max;
    }
}
