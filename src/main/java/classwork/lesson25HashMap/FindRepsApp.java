package classwork.lesson25HashMap;

import java.util.*;

public class FindRepsApp {
    static int[] findReps(int[] data) {
        int reps = 1;
        int[] d = new int[data.length];
        for (int i = 0; i < data.length; i++) {
            for (int j = i + 1; j < data.length; j++) {
                if (data[i] == data[j]) {
                    d[i] = reps++;
                }
            }
            reps = 1;
        }
        return d;
    }

    public static void main(String[] args) {
        int[] ints = new Random().ints(10,20).limit(20).toArray();

        System.out.println(Arrays.toString(ints));
        System.out.println(Arrays.toString(findReps(ints)));
    }
}
