package classwork.lesson05;


public class Sqr {
    public static void main(String[] args) {
        int[][] sqr = new int[7][7];
        int a = 1;
        for (int i = 0; i < sqr.length; i++){
            for (int j = 0; j < sqr.length; j++){
                if (i<=sqr.length/2 && j<=sqr.length/2) {
                    if (i < j) {
                        sqr[0][0] = i + 1;
                    } else {
                        sqr[i][j] = j + 1;
                    }
                }

                System.out.print(sqr[i][j]);
            }
            System.out.println();
        }

    }
}
