package classwork.lesson05;

import java.util.Arrays;

import java.util.Scanner;

public class Arr {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int SIZE = in.nextInt();
        int[] odd = new int[SIZE];
        int[] even = new int[SIZE];
        int[] merged = new int[SIZE*2];

        for (int i = 0; i < SIZE; i++) {
           even[i] = (int)(Math.random()*100)*2;
           odd[i] = (int)(Math.random()*100)*2+1;
        }

        for (int i = 0; i < SIZE; i++) {
            merged[i*2] = odd[i];
            merged[i*2+1] = even[i];
        }

        System.out.println(Arrays.toString(odd));
        System.out.println(Arrays.toString(even));
        System.out.println(Arrays.toString(merged));

    }
}
