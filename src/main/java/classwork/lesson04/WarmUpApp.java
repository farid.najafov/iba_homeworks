package classwork.lesson04;

public class WarmUpApp {
    public static boolean isCapital(char c) {
        return c >= 'A' && c <= 'Z';
    }

    public static boolean isSmall(char c) {
        return c >= 'a' && c <= 'z';
    }

    public static boolean isLetter(char c) {
        return isCapital(c) || isSmall(c);
    }

    public static boolean isVowel(char c) {
        final String vowels = "aeoiu";
        return vowels.indexOf(Character.toLowerCase(c)) >= 0;
    }

    public static boolean isConsonant(char c) {
        return !isVowel(c);
    }

    public static String alphabetSmall() {
        StringBuilder alpha = new StringBuilder();
        for (char i = 'a'; i <= 'z'; i++) {
            alpha.append(i);
        }
        return alpha.toString();
    }

    public static String alphabetCapital() {
        return alphabetSmall().toUpperCase();
    }

    public static char randomSmallLetter() {
        String alpha = alphabetSmall();
        return alpha.charAt((int) (Math.random()*alpha.length()));
    }

    public static char randomCapitalLetter() {
        String alpha = alphabetCapital();
        return alpha.charAt((int) (Math.random()*alpha.length()));
    }

    public static char randomLetter() {
        return ((int)(Math.random()*2)==0) ? // 0 or 1
                randomSmallLetter() : randomCapitalLetter();
    }

    public static void main(String[] args) {
//        1. generate a string of length 30 containing random small letters
        for (int i = 0; i < 30; i++) {
            System.out.print(randomSmallLetter());
        }
        System.out.println();

//        2. generate a string of length 25 containing random capital letters
        for (int i = 0; i < 25; i++) {
            System.out.print(randomCapitalLetter());
        }
        System.out.println();

//        3. generate a string of length 35 containing random mixed capital + small letters
        for (int i = 0; i < 35; i++) {
            System.out.print(randomLetter());
        }
        System.out.println();

//        4. generate a string of length 20 containing only vowels small letters
        int i = 0;
        while (i < 20){
            char a = randomSmallLetter();
            if (isVowel(a)){
                System.out.print(a);
            } else {
                continue;
            }
            i++;
        }
        System.out.println();

//        5. generate a string of length 20 containing only consonants small letters
        i = 0;
        while (i < 20){
            char a = randomSmallLetter();
            if (isConsonant(a)){
                System.out.print(a);
            } else {
                continue;
            }
            i++;
        }
        System.out.println();

//        6. generate a string of length 20 containing only vowels capital letters
        i = 0;
        while (i < 20){
            char a = randomCapitalLetter();
            if (isVowel(a)){
                System.out.print(a);
            } else {
                continue;
            }
            i++;
        }
        System.out.println();

//        7. generate a string of length 20 containing only consonants capital letters
        i = 0;
        while (i < 20){
            char a = randomCapitalLetter();
            if (isConsonant(a)){
                System.out.print(a);
            } else {
                continue;
            }
            i++;
        }
        System.out.println();

//        8. generate a string of length 20 containing only consonants capital and small letters
        i = 0;
        while (i < 20){
            char a = randomLetter();
            if (isConsonant(a)){
                System.out.print(a);
            } else {
                continue;
            }
            i++;
        }
        System.out.println();
    }
}

