package classwork.lesson07;

public class Triangle extends Figure {
    int p1;
    int p2;
    int p3;

    public static Triangle rand() {
        return new Triangle(Extra.rand(), Extra.rand(), Extra.rand());
    }

    public Triangle(int p1, int p2, int p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    @Override
    public int area() {
        return p1*p2*p3;
    }
}
