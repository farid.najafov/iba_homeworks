package classwork.lesson07;

public class Circle extends Figure {
    int rad;
    Point point;

    public static Circle rand() {
        return new Circle(Point.rand(), Extra.rand());
    }

    public  Circle(Point point, int rad) {
        this.point = point;
        this.rad =rad;
    }

    @Override
    public int area() {
        return point.x*point.y*rad;
    }
}
