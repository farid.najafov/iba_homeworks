package classwork.lesson07;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {


        Triangle triangle = new Triangle(3, 4, 5);
        Rectangle rectangle = new Rectangle(6,7);
        Circle circle = new Circle(new Point(3,5), 7);

        ArrayList<Figure> figures = new ArrayList<>();

//        figures.add(triangle);
//        figures.add(rectangle);
//        figures.add(circle);

//        figures.add(Rectangle.rand());
//        figures.add(Triangle.rand());
//        figures.add(Circle.rand());

        figures.add(Figure.rand());
        figures.add(Figure.rand());
        figures.add(Figure.rand());


        int total = 0;
        for (Figure f: figures)
            total += f.area();

        System.out.println(total);
    }
}
