package classwork.lesson07;

public class Figure {

    public static Figure rand() {
        switch ((int)(Math.random()*3)) {
            case 0: return Circle.rand();
            case 1: return Rectangle.rand();
            default:return Triangle.rand();
        }
    }

    public int area() {
        return 0;
    }

}
