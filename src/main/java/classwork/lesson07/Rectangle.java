package classwork.lesson07;

public class Rectangle extends Figure {
    int p1;
    int p2;

    public static Rectangle rand() {
        return new Rectangle(Extra.rand(), Extra.rand());
    }

    public Rectangle(int p1, int p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public int area() {
        return p1*p2;
    }
}
