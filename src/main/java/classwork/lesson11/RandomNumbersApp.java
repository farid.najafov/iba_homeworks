package classwork.lesson11;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomNumbersApp {
    public static void main(String[] args) {
        List<Integer> rand = new Random()
                .ints(20, -10,10)
                .boxed()
                .collect(Collectors.toList());

        List<Integer> first = rand.stream()
                .filter(x -> x < 0)
                .collect(Collectors.toList());

        List<Double> second = rand.stream()
                .filter(x -> x > 0)
                .map(Math::sqrt)
                .collect(Collectors.toList());

        System.out.println(rand);
        System.out.println(first);
        System.out.println(second);
    }
}
