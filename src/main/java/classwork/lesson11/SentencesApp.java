package classwork.lesson11;

import java.util.ArrayList;
import java.util.List;

public class SentencesApp {
    public static void main(String[] args) {
        List<String> subjects = list("Noel", "The cat", "The dog");
        List<String> verbs = list("wrote", "chased", "slept on");
        List<String> objects = list("the book", "the ball", "the bed");


        for (String subject : subjects) {
            for (String verb : verbs) {
                for (String object : objects) {
                    System.out.printf("%s %s %s\n", subject, verb, object);
                }
            }
        }
    }

    private static List<String> list(String a, String b, String c) {
        List<String> abc = new ArrayList<>();
        abc.add(a);
        abc.add(b);
        abc.add(c);
        return abc;
    }
}
