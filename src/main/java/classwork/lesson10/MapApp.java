package classwork.lesson10;

import java.util.HashMap;
import java.util.Map;

public class MapApp {
    public static void main(String[] args) {
        String s = "Hello World";

        char[] charArr = s.toCharArray();

        HashMap<Character, Integer> countChar = new HashMap<>();

        for (char c: charArr) {
            if (!countChar.containsKey(c)) {
                countChar.put(c, 1);
            } else {
                countChar.put(c, countChar.get(c) + 1);
            }
        }

        for (Map.Entry entry : countChar.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }







//
//            int[] count = new int[255];
//
//            for (int i = 0; i < s.length(); i++) {
//                count[s.charAt(i)]++;
//            }
//
//            char[] ch = new char[s.length()];
//            for (int i = 0; i < s.length(); i++) {
//                ch[i] = s.charAt(i);
//                int x = 0;
//                for (int j = 0; j <= i; j++) {
//                    if (s.charAt(i) == ch[j])
//                        x++;
//                }
//
//                if (x == 1) {
//                    System.out.println(s.charAt(i) + ": " + count[s.charAt(i)]);
//                }
//            }
        }
    }
