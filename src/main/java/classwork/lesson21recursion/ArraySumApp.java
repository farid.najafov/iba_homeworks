package classwork.lesson21recursion;

import java.util.Arrays;
import java.util.Random;

public class ArraySumApp {
    static int sum_iterative(int[] data) {
        int sum = 0;
        for (int datum : data) {
            sum += datum;
        }
        return sum;
    }

    // head recursion, implementation
    static int sum_headRec(int[] data, int idx) {
        if (idx == data.length) return 0;
        int acc = sum_headRec(data, idx + 1);
        return data[idx] + acc;
    }
    // head recursion, runner
    static int sum_headRec(int[] data) {
        return sum_headRec(data, 0);
    }

    // tail recursion, implementation
    static int sum_tailRec(int[] data, int idx, int acc) {
        if (idx == data.length) return acc;
        int newAcc = acc + data[idx];
        return sum_tailRec(data, idx + 1, newAcc);
    }
    // tail recursion, runner
    static int sum_tailRec(int[] data) {
        return sum_tailRec(data, 0, 0);
    }

    // tail recursion, implementation
    static int sum_tailRec1(int[] data, int idx, int result) {
        if (idx == data.length) return result;
        return sum_tailRec(data, idx + 1, data[idx] + result);
    }
    // tail recursion, runner
    static int sum_tailRec1(int[] data) {
        return sum_tailRec1(data, 0, 0);
    }

    public static void main(String[] args) {
        int[] ints = new Random().ints(1, 10).limit(10).toArray();
        System.out.println(Arrays.toString(ints));
        System.out.println( sum_iterative(ints) );
        System.out.println( sum_headRec(ints) );
        System.out.println( sum_tailRec(ints) );
        System.out.println( sum_tailRec1(ints) );

    }
}
