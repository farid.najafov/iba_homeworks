package classwork.lesson21recursion;

public class SumFrom1toNApp {
    public static int sumHeadRec(int n) {
        if (n == 1) return 1;
        return n + sumHeadRec(n - 1);
    }

    public static int sumTailRec(int n, int acc) {
        if (n == 0) return acc;
        return sumTailRec(n - 1, acc + n);
    }
    public static int sumTailRec(int n) {
        return sumTailRec(n, 0);
    }

    public static void main(String[] args) {
        System.out.println( sumHeadRec(5) );
        System.out.println( sumTailRec(5) );
    }
}
