package classwork.lesson21recursion;

public class FactorialApp {
    static int factorial_iterative(int n) {
        if (n < 0) throw new IllegalArgumentException("number less than zero given");
        int result = 1;
        for (int next = 2; next <= n; next++) {
            result = result * next;
        }
        return result;
    }
    static int factorialHeadRec(int n) {
        if (n < 0) throw new IllegalArgumentException("number less than zero given");
        if (n <= 1) return 1;
        return n * factorialHeadRec(n - 1);
    }

    static int factorialTailRec(int n, int acc) {
        if (n <= 1) return acc;
        return factorialTailRec(n - 1, n * acc);
    }
    static int factorialTailRec(int n) {
        if (n < 0) throw new IllegalArgumentException("number less than zero given");
        return factorialTailRec(n, 1);
    }

    public static void main(String[] args) {
        System.out.println( factorial_iterative(5) );
        System.out.println( factorialHeadRec(5) );
        System.out.println( factorialTailRec(5) );
    }
}
