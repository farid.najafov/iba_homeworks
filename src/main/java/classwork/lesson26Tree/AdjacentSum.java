package classwork.lesson26Tree;

import java.util.Arrays;
import java.util.Random;

public class AdjacentSum {
    static void adjacentSum(int[] data) {
        int sum;
        int[] arr = new int[data.length - 1];
        for (int i = 0; i < data.length - 1; i++) {
            sum = data[i] + data[i + 1];
            arr[i] = sum;
        }
        int min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) min = arr[i];
        }
        System.out.println("Sum: " + min);
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == min) index = i;
        }
        System.out.println("Index1: " + index);
        System.out.println("Index2: " + (index + 1));
    }

    public static void main(String[] args) {
        int[] arr = new Random().ints(10,50).limit(30).toArray();
        System.out.println(Arrays.toString(arr));
        adjacentSum(arr);
    }
}
