package classwork.lesson12;

import java.io.*;

import java.util.*;


public class WarmUp {
    public static void main(String[] args) throws IOException {
        File firstFile = new File("src/main/java/classwork/lesson12/subj_verb.txt");
        File secondFile =new File("src/main/java/classwork/lesson12/subj_verb.txt");

        HashMap<String, List<String>> firstMap = new HashMap<>();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(firstFile));
        while ((line = reader.readLine()) != null) {
            String[] parts = line.split(":");
                String key = parts[0];
                String value = parts[1];
                firstMap.put(key, Collections.singletonList(value));
        }
        for (String key : firstMap.keySet()) {
            System.out.println(key + ":" + firstMap.get(key));
        }

        HashMap<String, List<String>> secondMap = new HashMap<>();
        String ln;
        BufferedReader reader2 = new BufferedReader(new FileReader(secondFile));
        while ((line = reader2.readLine()) != null) {
            String[] parts = line.split(":");
            String key = parts[0];
            String value = parts[1];
            firstMap.put(key, Collections.singletonList(value));
        }
        for (String key : firstMap.keySet()) {
            System.out.println(key + ":" + firstMap.get(key));
        }

    }
}
