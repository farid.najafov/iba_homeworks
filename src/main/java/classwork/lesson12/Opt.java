package classwork.lesson12;

import java.util.Optional;

public class Opt {


    public static void main(String[] args) {
        Optional<Integer> value1 = strToInt("123");
        Optional<Integer> value2 = strToInt("123q");

    }

    private static Optional<Integer> strToInt(String s) {
        try {
            return Optional.of(Integer.parseInt(s));

        } catch (NumberFormatException e) {
            return Optional.empty();
        }

    }
}
