package classwork.lesson09.Warmup2;

public class Formatter2_ extends Formatter0_ {
    public Formatter2_(String msg) {
        super(msg);
    }

    @Override
    protected String format(String origin) {
        return origin.toUpperCase();
    }
}
