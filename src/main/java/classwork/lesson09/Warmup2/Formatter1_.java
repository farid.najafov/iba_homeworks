package classwork.lesson09.Warmup2;

public class Formatter1_ extends Formatter0_{
    public Formatter1_(String msg) {
        super(msg);
    }

    @Override
    protected String format(String origin) {
        return origin.toLowerCase();
    }
}
