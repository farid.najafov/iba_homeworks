package classwork.lesson09.Warmup2;

public abstract class Formatter0_ {
    protected final String msg;

    public Formatter0_(String msg) {
        this.msg = msg;
    }

    abstract protected String format(String origin);

    @Override
    public String toString() {
        return format(msg);
    }
}
