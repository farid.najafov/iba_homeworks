package classwork.lesson09.WarmUp3;

public class FormatterApp3 {
    public static void main(String[] args) {
        Formatter_0 fmt1 = new Formatter_1();
        Formatter_0 fmt2 = new Formatter_2();
        Formatter_0 fmt3 = new Formatter_3();

        // task1
        print("Hello", fmt1); // hello
        print("Hello", fmt2); // HELLO
        print("Hello", fmt3); // ***********
        // *  Hello  *
        // ***********
    }

    static void print(String message, Formatter_0 fmt) {
        String formatted = fmt.format(message);
        System.out.println(formatted);
    }
}

