package classwork.lesson09.WarmUp3;

public class Formatter_3 implements Formatter_0{
    @Override
    public String format(String origin) {
        StringBuilder sb = new StringBuilder();
        sb.append("***********\n");
        sb.append("*  ");
        sb.append(origin);
        sb.append("  *\n");
        sb.append("***********\n");

        return sb.toString();
    }
}
