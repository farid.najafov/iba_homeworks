package classwork.lesson19sorting_algo;

import java.util.Comparator;

public class SizeCompareApple implements Comparator<Apple> {
    @Override
    public int compare(Apple apple, Apple t1) {
        if (apple.size < t1.size) return -1;
        if (apple.size > t1.size) return 1;
        else return 0;
    }
}
