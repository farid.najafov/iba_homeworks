package classwork.lesson19sorting_algo;

public class ShoesPairs {
    public int calc(String s) {
        int shoes = 0;
        int pairs = 0;
        for (int i = 0; i < s.length(); i++ ){
            switch (s.charAt(i)) {
                case 'R': shoes++; break;
                case 'L': shoes--; break;
            }
            if (shoes == 0) pairs++;
        }
        return pairs;
    }

    public static void main(String[] args) {

    }
}
