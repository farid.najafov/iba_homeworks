package classwork.lesson19sorting_algo;

import java.util.Arrays;
import java.util.Random;

public class BubbleSortApp {
    public static void sort(int[] a) {
        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            for (int i = 0; i < a.length - 1; i++) {
                if (a[i] > a[i+1]) {
                    int tmp = a[i];
                    a[i] = a[i+1];
                    a[i+1] = tmp;
                    sorted = false;
                }
            }
        }
    }

    public static void sortA(int[] a) {
        for (int i = 0; i < a.length ; i++) {
            for (int j = i; j < a.length; j++) {
                if (a[i] > a[j]) {
                    int tmp = a[i];
                    a[i] = a[j];
                    a[j] = tmp;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = new Random().ints(1, 99).limit(20).toArray();
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));
        sortA(arr);
        System.out.println(Arrays.toString(arr));
    }
}
