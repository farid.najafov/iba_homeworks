package classwork.lesson19sorting_algo.extra;

import java.util.Arrays;
import java.util.Random;

public class SortApp {
    public static void sort(int[] ints) {
        for (int i = 0; i < ints.length; i++) {
            int min = ints[i];
            int idx = i;
            for (int j = i + 1; j < ints.length; j++) {
                if (ints[j] < min) {
                    min = ints[j];
                    idx = j;
                }
            }
            if (i != idx) {
                int tmp = ints[i];
                ints[i] = min;
                ints[idx] = tmp;
            }
        }
    }
    public static void main(String[] args) {
        int[] arr = new Random().ints(1, 30).limit(20).toArray();
        System.out.println(Arrays.toString(arr));
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
