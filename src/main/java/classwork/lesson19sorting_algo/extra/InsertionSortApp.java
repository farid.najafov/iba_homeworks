package classwork.lesson19sorting_algo.extra;

import java.util.Arrays;
import java.util.Random;

public class InsertionSortApp {
    static void insertionSort(int[] ints) {
        for (int i = 1; i < ints.length; i++) {
            int curr = ints[i];
            int j = i - 1;
            while (j >= 0 && ints[j] > curr) {
                ints[j + 1] = ints[j];
                j--;
            }
            ints[j + 1] = curr;
        }
    }
    public static void main(String[] args) {
        int[] ints = new Random().ints(0,30).limit(10).toArray();
        System.out.println(Arrays.toString(ints));
        insertionSort(ints);
        System.out.println(Arrays.toString(ints));
    }
}
