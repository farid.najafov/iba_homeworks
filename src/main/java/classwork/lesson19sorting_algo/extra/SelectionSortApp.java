package classwork.lesson19sorting_algo.extra;

import java.util.Arrays;
import java.util.Random;

public class SelectionSortApp {
    static void selectionSort(int[] data) {
        for (int i = 0; i < data.length; i++) {
            int min = data[i];
            int idx = i;
            for (int j = i + 1; j < data.length; j++) {
                if (data[j] < min) {
                    min = data[j];
                    idx = j;
                }
            }
            int tmp = data[i];
            data[i] = min;
            data[idx] = tmp;
        }
    }
    public static void main(String[] args) {
        int[] ints = new Random().ints(0, 30).limit(20).toArray();
        System.out.println(Arrays.toString(ints));
        selectionSort(ints);
        System.out.println(Arrays.toString(ints));
    }
}
