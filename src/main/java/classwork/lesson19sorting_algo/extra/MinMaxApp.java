package classwork.lesson19sorting_algo.extra;

import java.util.Arrays;
import java.util.Random;

public class MinMaxApp {
    static int[] minMax(int[] ints) {
        int min = ints[0];
        int max = ints[0];

        for (int i = 1; i < ints.length; i++) {
            if (ints[i] < min) min = ints[i];
            if (ints[i] > max) max = ints[i];
        }
        return new int[]{min, max};
    }

    public static void main(String[] args) {
        int[] ints = new Random().ints(-100, 100 ).limit(10).toArray();
        System.out.println(Arrays.toString(ints));
        int[] mm = minMax(ints);
        System.out.println(Arrays.toString(mm));
    }
}
