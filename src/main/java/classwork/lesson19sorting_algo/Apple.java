package classwork.lesson19sorting_algo;

public class Apple {
    public final int size;

    public int getSize() {
        return size;
    }

    public Apple(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return String.format("A [%d]", size);
    }
}
