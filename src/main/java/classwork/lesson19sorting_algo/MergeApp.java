package classwork.lesson19sorting_algo;

import java.util.*;
import java.util.stream.Collectors;

public class MergeApp {
    static int[] merge(int[] data1, int[] data2) {
        int[] data3 = new int[data1.length + data2.length];
        int idx1 = 0;
        int idx2 = 0;
        int idx3 = 0;

        while (idx1 < data1.length && idx2 < data2.length) {
            data3[idx3++] = (data1[idx1] < data2[idx2]) ? data1[idx1++] : data2[idx2++];
        }
        while (idx1 < data1.length) {
            data3[idx3++] = data1[idx1++];
        }
        while (idx2 < data2.length) {
            data3[idx3++] = data2[idx2++];
        }
        return data3;
    }
    // V1
    static List<Apple> mergeApples1(List<Apple> list1, List<Apple> list2) {
        List<Apple> list3 = new ArrayList<>();
        int idx1 = 0;
        int idx2 = 0;
        while (idx1 < list1.size() && idx2 < list2.size()) {
            if (list1.get(idx1).size < list2.get(idx2).size) {
                list3.add(list1.get(idx1++));
            } else list3.add(list2.get(idx2++));
        }
        while (idx1 < list1.size()) {
            list3.add(list1.get(idx1++));
        }
        while (idx2 < list2.size()) {
            list3.add(list2.get(idx2++));
        }
        return list3;
    }
    // V2
    public static List<Apple> mergeApples2(List<Apple> list1, List<Apple> list2, Comparator<Apple> cmp) {
        ArrayList<Apple> apples3 = new ArrayList<>();
        int idx1 = 0;
        int idx2 = 0;
        while (idx1 < list1.size() && idx2 < list2.size()) {
            apples3.add(
                    cmp.compare(list1.get(idx1), list2.get(idx2)) < 0 ?
                            list1.get(idx1++) : list2.get(idx2++)
            );
        }
        while (idx1 < list1.size()) {
            apples3.add( list1.get(idx1++) );
        }
        while (idx2 < list2.size()) {
            apples3.add( list2.get(idx2++) );
        }
        return apples3;
    }
    // V3
    public static <A> List<A> mergeApples3(List<A> list1, List<A> list2, Comparator<A> cmp) {
        List<A> apples3 = new ArrayList<>();
        int idx1 = 0;
        int idx2 = 0;
        while (idx1 < list1.size() && idx2 < list2.size()) {
            apples3.add(
                    cmp.compare(list1.get(idx1), list2.get(idx2)) < 0 ?
                            list1.get(idx1++) : list2.get(idx2++)
            );
        }
        while (idx1 < list1.size()) {
            apples3.add( list1.get(idx1++) );
        }
        while (idx2 < list2.size()) {
            apples3.add( list2.get(idx2++) );
        }
        return apples3;
    }

    public static void main(String[] args) {
//        int[] d1 = {1,2,3};
//        int[] d2 = {10,11}; // 1,2,3,10,11
//        System.out.println(Arrays.toString(merge(d1,d2)));

//        List<Apple> apples1 = new Random().ints(0, 20).limit(10)
//                .mapToObj(n -> new Apple(n)).sorted().collect(Collectors.toList());
//        List<Apple> apples2 = new Random().ints(0, 20).limit(10)
//                .mapToObj(n -> new Apple(n)).sorted().collect(Collectors.toList());
//        List<Apple> apples3 = mergeApples1(apples1, apples2);
//        System.out.println(apples1);
//        System.out.println(apples2);
//        System.out.println(apples3);

//        SizeCompareApple sc = new SizeCompareApple();
        Comparator<Apple> appleComparator = (o1, o2) -> o1.size - o2.size;
        Comparator<Apple> appleComparator2 = Comparator.comparing(s -> s.size); // or (Apple::getSize)
        List<Apple> apples11 = new Random().ints(0, 20).limit(10)
                .mapToObj(n -> new Apple(n)).sorted(appleComparator2).collect(Collectors.toList());
        List<Apple> apples12 = new Random().ints(0, 20).limit(10)
                .mapToObj(n -> new Apple(n)).sorted(appleComparator2).collect(Collectors.toList());
        System.out.println(apples11);
        System.out.println(apples12);
        List<Apple> apples13 = mergeApples1(apples11, apples12); // V1
        System.out.println(apples13);
        List<Apple> apples14 = mergeApples2(apples11, apples12, appleComparator); // V2
        System.out.println(apples14);
        List<Apple> apples15 = mergeApples3(apples11, apples12, appleComparator); // V3
        System.out.println(apples15);

    }
}
