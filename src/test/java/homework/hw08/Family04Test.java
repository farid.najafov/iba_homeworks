package homework.hw08;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Family04Test {
    Human04 firstChild;
    Human04 secondChild;
    Family04 family;
    Human04 father;
    Human04 mother;

    @BeforeEach
    void setUp() {
        firstChild = new Man04("Leo", "Tyler", 2000);
        secondChild = new Woman04("Kate", "Tyler", 2005);
        family = new Family04();
        father = new Man04("John", "Travolta", 1950);
        mother = new Woman04("Jenny", "Oldstones", 1970);
        family.addChild(firstChild);
    }

    @Test
    void addChild() {
        int expected = family.getChildren().size() + 1;
        family.addChild(secondChild);
        int actual = family.getChildren().size();
        assertEquals(expected, actual);
    }

    @Test
    void testDeleteChildIfCorrectObjectIsPassed() {
        assertTrue(family.deleteChild(firstChild));
    }

    @Test
    void testDeleteChildIfWrongObjectIsPassed() {
        assertFalse(family.deleteChild(secondChild));
    }

    @Test
    void countFamily() {
        int expected = family.countFamily() + 1;
        family.addChild(secondChild);
        int actual = family.countFamily();
        assertEquals(expected, actual);
    }
}