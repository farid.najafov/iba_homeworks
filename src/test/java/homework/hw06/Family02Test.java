package homework.hw06;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Family02Test {
    Human02 father;
    Human02 mother;
    Human02 firstChild;
    Pet02 cat;
    Human02[] children;
    Family02 newFamily;
    Human02 secondChild;

    @BeforeEach
    void setUp() {
        this.father = new Human02("Brad", "Pitt", 1960);
        this.mother = new Human02("Angelina", "Jolie", 1970);
        this.firstChild = new Human02("Maddox", "Jolie-Pitt", 2000, (byte) 90);
        this.cat = new Pet02(Species.CAT, "Kitty", 5, (byte)75, new String[]{"eat", "drink", "sleep"});
        this.children = new Human02[]{firstChild};
        this.newFamily = new Family02(mother, father, children, cat);
        this.secondChild = new Human02("Vivienne", "Jolie-Pitt", 2000, (byte) 90);
    }

    @Test
    void addChild() {
        int expected = newFamily.getChildren().length + 1;
        newFamily.addChild(secondChild);
        int actual = newFamily.getChildren().length;
        assertEquals(expected, actual);
    }

    @Test
    void testDeleteChildIfCorrectObjectIsPassed() {
        assertTrue(newFamily.deleteChild(firstChild));
    }

    @Test
    void testDeleteChildIfWrongObjectIsPassed() {
        assertFalse(newFamily.deleteChild(secondChild));
    }

    @Test
    void countFamily() {
        int  expected = newFamily.countFamily();
        int actual= 2 + newFamily.getChildren().length;
        assertEquals(expected, actual);
    }

    @Test
    void equals() {
        Family02 testFamily = new Family02();
        assertTrue(!newFamily.equals(testFamily));
    }
}