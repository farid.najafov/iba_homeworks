package homework.hw09.Service;

import homework.hw09.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    FamilyService fs;
    Human fatherRey;
    Human motherRey;
    Human firstChildRey;
    List<Human> childrenRey;
    Dog myDog;
    Set<Pet> petsOfReynolds;
    Family fmReynolds;

    Human fatherEast;
    Human motherEast;
    Human firstChildEast;
    List<Human> childrenEast;
    Fish myFish;
    Set<Pet> petsOfEastwoods;
    Family fmEastwoods;


    @BeforeEach
    void setUp() {
        fs = new FamilyService();
        fatherRey = new Man("Ryan", "Reynolds", 1980);
        motherRey = new Woman("Blake", "Lively", 1990);
        firstChildRey = new Man("James", "Reynolds", 2010);
        childrenRey = new ArrayList<>();
        childrenRey.add(firstChildRey);
        myDog = new Dog("Rock");
        petsOfReynolds = new HashSet<>();
        petsOfReynolds.add(myDog);
        fmReynolds = new Family(motherRey, fatherRey, childrenRey, petsOfReynolds);

        fatherEast = new Man("Clint", "Eastwood", 1940);
        motherEast = new Woman("Dina", "Eastwood", 1960);
        firstChildEast = new Man("Scott", "Eastwood", 1990);
        childrenEast = new ArrayList<>();
        childrenEast.add(firstChildEast);
        myFish = new Fish("Nemo");
        petsOfEastwoods= new HashSet<>();
        petsOfEastwoods.add(myFish);
        fmEastwoods = new Family(motherEast, fatherEast, childrenEast, petsOfEastwoods);
        fs.saveFamily(fmEastwoods);
    }

    @Test
    void getAllFamilies() {
        assertTrue(fs.getAllFamilies().contains(fmEastwoods));
    }

    @Test
    void saveFamily() {
        fs.saveFamily(fmReynolds);
        assertTrue(fs.getAllFamilies().contains(fmReynolds));
    }

    @Test
    void getFamiliesBiggerThan() {
        int expected = 1;
        int actual = fs.getFamiliesBiggerThan(1).size();
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesLessThan() {
        int expected = 1;
        int actual = fs.getFamiliesLessThan(4).size();
        assertEquals(expected, actual);
    }

    @Test
    void countFamiliesWithMemberNumber() {
        int expected = 1;
        int actual = fs.countFamiliesWithMemberNumber(3);
        assertEquals(expected, actual);
    }

    @Test
    void createNewFamily() {
        int expected = fs.getAllFamilies().size() + 1;
        fs.createNewFamily(motherEast, fatherEast);
        int actual = fs.getAllFamilies().size();
        assertEquals(expected, actual);
    }

    @Test
    void deleteFamilyByIndex() {
        assertTrue(fs.deleteFamilyByIndex(0));
    }

    @Test
    void deleteFamily() {
        assertTrue(fs.deleteFamily(fmEastwoods));
    }

    @Test
    void bornChild() {
        int expected = fmEastwoods.getChildren().size() + 1;
        fs.bornChild(fmEastwoods, "AA", "BB");
        int actual = fmEastwoods.getChildren().size();
        assertEquals(expected, actual);
    }

    @Test
    void adoptChild() {
        int expected = fmEastwoods.getChildren().size() + 1;
        fs.adoptChild(fmEastwoods, firstChildRey);
        int actual = fmEastwoods.getChildren().size();
        assertEquals(expected, actual);
    }

    @Test
    void deleteAllChildrenOlderThan() {
        fs.deleteAllChildrenOlderThan(0);
        assertTrue(fmEastwoods.getChildren().isEmpty());
    }

    @Test
    void count() {
        assertEquals(1, fs.count());
    }

    @Test
    void getFamilyById() {
        assertEquals(fmEastwoods, fs.getFamilyById(0));
    }

    @Test
    void getPets() {
        assertSame(fs.getPets(0), petsOfEastwoods);
    }

    @Test
    void addPet() {
        fs.addPet(0, myDog);
        assertTrue(fmEastwoods.getPet().contains(myDog));
    }
}