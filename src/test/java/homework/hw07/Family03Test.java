package homework.hw07;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Family03Test {
    Human03 father;
    Human03 mother;
    Human03 firstChild;
    DomesticCat myCat;
    Human03[] children;
    Family03 newFamily;
    Human03 secondChild;

    @BeforeEach
    void setUp() {
        this.father = new Human03("Brad", "Pitt", 1960);
        this.mother = new Human03("Angelina", "Jolie", 1970);
        this.firstChild = new Human03("Maddox", "Jolie-Pitt", 2000);
        this.myCat = new DomesticCat("Kitty", 5, (byte) 15, new String[]{"eat", "drink", "sleep"});
        this.children = new Human03[]{firstChild};
        this.newFamily = new Family03(mother, father, children, myCat);
        this.secondChild = new Human03("Vivienne", "Jolie-Pitt", 2000);
    }

    @Test
    void addChild() {
        int expected = newFamily.getChildren().length + 1;
        newFamily.addChild(secondChild);
        int actual = newFamily.getChildren().length;
        assertEquals(expected, actual);
    }

    @Test
    void testDeleteChildIfCorrectObjectIsPassed() {
        assertTrue(newFamily.deleteChild(firstChild));
    }

    @Test
    void testDeleteChildIfWrongObjectIsPassed() {
        assertFalse(newFamily.deleteChild(secondChild));
    }

    @Test
    void countFamily() {
        int  expected = newFamily.countFamily();
        int actual= 2 + newFamily.getChildren().length;
        assertEquals(expected, actual);
    }
}