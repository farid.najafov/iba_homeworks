package homework.hw13.Service;

import homework.hw13.DAO.CollectionFamilyDAO;
import homework.hw13.entity.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    FamilyService fs;
    Family fmReynolds;
    Family fmEastwoods;
    Human firstChildRey;
    Human fatherEast;
    Human motherEast;

    @BeforeEach
    public void testData() {
        fs = new FamilyService();
        //      =========================== create family Reynold ====================================
        // create pets
        DomesticCat myCat = new DomesticCat("Kitty");
        Dog myDog = new Dog("Rock");
        Set<Pet> petsOfReynolds = new HashSet<>();
        petsOfReynolds.add(myCat);
        petsOfReynolds.add(myDog);

        // create father and mother
        Human fatherRey = new Man("Ryan", "Reynolds", "01/01/1990");
        Human motherRey = new Woman("Blake", "Lively", "01/01/1990");

        // create children
        firstChildRey = new Man("James", "Reynolds", "01/01/2010");
        Human secondChildRey = new Woman("Inez", "Reynolds", "01/01/2015");
        List<Human> childrenRey = new ArrayList<>();
        childrenRey.add(firstChildRey);
        childrenRey.add(secondChildRey);

        // create family
        fmReynolds = new Family(motherRey, fatherRey, childrenRey, petsOfReynolds);

        //      =========================== create family Eastwoods ====================================
        // create pets
        Fish myFish = new Fish("Nemo");
        RoboCat myRoboCat = new RoboCat("Bob");
        Set<Pet> petsOfEastwoods = new HashSet<>();
        petsOfEastwoods.add(myFish);
        petsOfEastwoods.add(myRoboCat);

        // create father and mother
        fatherEast = new Man("Clint", "Eastwood", "01/01/1940");
        motherEast = new Woman("Dina", "Eastwood", "01/01/1960");

        // create children
        Human firstChildEast = new Man("Scott", "Eastwood", "01/01/1990");
        Human secondChildEast = new Woman("Morgan", "Eastwood", "01/01/2000");
        List<Human> childrenEast = new ArrayList<>();
        childrenEast.add(firstChildEast);
        childrenEast.add(secondChildEast);

        // create family
        fmEastwoods = new Family(motherEast, fatherEast, childrenEast, petsOfEastwoods);

        List<Family> families = new ArrayList<>();
        families.add(fmReynolds);
        families.add(fmEastwoods);
        CollectionFamilyDAO dao = new CollectionFamilyDAO();
        dao.write(families);
    }

    @Test
    void saveFamily() throws ParseException {
        fmReynolds = new Family(null, null);
        fs.saveFamily(fmReynolds);
        assertTrue(fs.getAllFamilies().contains(fmReynolds));
    }

    @Test
    void getFamiliesBiggerThan() throws ParseException {
        int expected = 2;
        int actual = fs.getFamiliesBiggerThan(1).size();
        assertEquals(expected, actual);
    }

    @Test
    void getFamiliesLessThan() throws ParseException {
        int expected = 2;
        int actual = fs.getFamiliesLessThan(5).size();
        assertEquals(expected, actual);
    }

    @Test
    void countFamiliesWithMemberNumber() throws ParseException {
        int expected = 2;
        int actual = fs.countFamiliesWithMemberNumber(4);
        assertEquals(expected, actual);
    }

    @Test
    void createNewFamily() throws ParseException {
        int expected = fs.getAllFamilies().size() + 1;
        fs.createNewFamily(motherEast, fatherEast);
        int actual = fs.getAllFamilies().size();
        assertEquals(expected, actual);
    }

    @Test
    void bornChild() throws ParseException {
        int expected = fmEastwoods.getChildren().size() + 1;
        fs.bornChild(fmEastwoods, "AA", "BB");
        int actual = fmEastwoods.getChildren().size();
        assertEquals(expected, actual);
    }

    @Test
    void adoptChild() throws ParseException {
        int expected = fmEastwoods.getChildren().size() + 1;
        fs.adoptChild(fmEastwoods, firstChildRey);
        int actual = fmEastwoods.getChildren().size();
        assertEquals(expected, actual);
    }

    @Test
    void count() throws ParseException {
        assertEquals(2, fs.count());
    }

}